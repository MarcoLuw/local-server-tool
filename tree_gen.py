from anytree import Node, RenderTree, NodeMixin

def tach_chuoi(raw_data):
    # print(txt_file)
    rows = raw_data.split("\n")
    data1 = [list(row.split()) for row in rows][:-1]
    # print(data1)
    if data1[0][0] == '/': 
        data1[0][0] = '/root'

    for i in range(1,len(data1)):
        if data1[0][0] == '/root':
            data1[i][0] = '/root' + data1[i][0]
        data1[i][0] = data1[i][0].split("/")[1:]
    
    data1[0][0] = data1[0][0].split('/')[1:]
    if len(data1[0][0]) != 1:
        data1[0][0] = '/' + '/'.join(data1[0][0])
    
    # print(data1)
    # print("------------------------------------------")
    return data1

class myClass(NodeMixin):
    def __init__(self, name, length, parent = None, children = None):
        super(myClass, self).__init__()
        self.name = name
        self.length = length
        self.parent = parent
        if children:    self.children = children

def change_format(byte_number):
    units = ['B', 'MB', 'GB', 'TB', 'PB']
    size = int(byte_number)
    for i in range(len(units)):
        if size < 1024 or i == len(units) - 1:
            break
        size /= 1024
    size = round(size, 2)
    return f'{size} {units[i]}'


def construct_tree(raw_data):
    data = tach_chuoi(raw_data)
    nodes = []
    root_node = myClass(name=data[0][0][0] if len(data[0][0]) == 1 else data[0][0], length=data[0][1])
    nodes.append(root_node)
    # print(root_node.length)
    for i in range(1, len(data)):
        node_gen = myClass(name=data[i][0][len(data[i][0])-1], length=data[i][1], parent=nodes[len(nodes)-1])
        nodes.append(node_gen)
    
    result = ""
    for pre, _, node in RenderTree(root_node):
        # treestr = u"%s%s" % (pre, node.name)
        # print(treestr, node.length)
        result += f'{pre}{node.name} {change_format(node.length)} \n'
    return result

# Return root node
def construct_root(raw_data):
    data = tach_chuoi(raw_data)
    nodes = []
    root_node = myClass(name=data[0][0][0] if len(data[0][0]) == 1 else data[0][0], length=data[0][1])
    nodes.append(root_node)
    # print(root_node.length)
    for i in range(1, len(data)):
        node_gen = myClass(name=data[i][0][len(data[i][0])-1], length=data[i][1], parent=nodes[len(nodes)-1])
        nodes.append(node_gen)
    
    return root_node

# json data for model
def node_to_json(node):
    json_data = {
        'label': node.name,
        'value': node.length,
        'children': []
    }
    if node.children:
        for child in node.children:
            json_data['children'].append(node_to_json(child))
    return [json_data]