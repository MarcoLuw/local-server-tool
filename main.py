import sys
import random
#from PySide6 import QtCore, QtWidgets, QtGui, QtQuick, QtQml
from PySide2.QtCore import QObject, QProcess, Signal, Slot, QUrl
from PySide2.QtGui import QGuiApplication, QIcon
from PySide2.QtQml import QQmlApplicationEngine
from PySide2 import QtWidgets
import backend


def create_app():
    app = QtWidgets.QApplication(sys.argv)
    engine = QQmlApplicationEngine()

    # # Create an instance of the backend and register it with the engine
    engine.load(QUrl.fromLocalFile('A://HoangAnh//local-server-tool//main.qml'))

    my_backend = backend.Backend(engine)
    engine.rootContext().setContextProperty("backend", my_backend)
    app.setWindowIcon(QIcon("A://HoangAnh//local-server-tool//image//akselos_square.png"))

    if not engine.rootObjects():
        sys.exit(-1)

    sys.exit(app.exec_())

    # -- Checking connecting state after each 1 min -- #
    
    

if __name__ == "__main__":
    create_app()