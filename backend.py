import sys
import os
import subprocess
import paramiko as pk
from PySide2.QtCore import QObject, QProcess, Signal, Slot, Property
import json
import threading
import socket
import time
import datetime
import utils

INTERVAL = 3000

class Backend(QObject):
    passwd = "bethaongoc"
    ssh_tracker = []
    
    disk_header = []

    client = None
    connection_lst = {}
    disk_tree_output = ""
    interval = INTERVAL
    largest_dir_model = []

    job_chart_model = []               # -- Line Chart Model -- #
    job_pie_chart_model = []           # -- Pie Chart Model -- #
    ncpu_pie_chart_model = []          # -- Pie Chart Model -- #
    interval_chart_model = []

    # Change using arg parser
    action_command = {
        'check-disk': 'df -h',
        'check-disk-filter': "df -h | awk 'NR==1 || (NR>1 && ($6 == \"/\" || $6 ~ /^\/mnt/)) {if ($5+0 > 90) {printf \"%s\\n\", $0} else {print}}'",
        # 'check-disk-filter': "df -h | awk 'NR==1 || (NR>1) {if ($5+0 > 90) {printf \"%s\\n\", $0} else {print}}'",
        'check-who-connecting': 'who -u',
        'check-https-port': 'cat akselos-dev/akselos_config.json',
        'check-hosting-serivce': '',
        'check-service-quickview': 'service --status-all',
        'check-all-service': 'systemctl list-units --type=service',
        'check-who-by-log': 'tail /var/log/auth.log',
        'check-who-by-network': 'netstat -tn | grep ESTABLISHED',
        'check-running-jobs-inshell': 'jobs',
        'check-running-jobs-allserver': 'ps -ef',
        'check-running-jobs-curuser': 'ps -ef | grep $$',
        'check-hostname': 'hostname',
        'custom-input': ''
    }

    def __init__(self, engine):
        QObject.__init__(self)
        self.engine = engine
        self.server_state_model = self.get_initial_server_state()
    """ # side function here """

    # -- Working with data.json file -- #
    def get_data_json(self):
        # Construct the relative file path
        file_path = os.path.join(os.path.dirname(__file__), 'data.json')
        with open(file_path, "r") as file:
            data = json.load(file)
        return data
    
    def write_file(self, data_json):
        # Construct the relative file path
        file_path = os.path.join(os.path.dirname(__file__), 'data.json')
        with open(file_path, "w") as file:
            json.dump(data_json, file, indent=4)


    """--- Code for connecting and checking server state -----"""
    """-------------------------------------------------------"""

    #-- Adding server --#
    # change into add_server
    @Slot(str, str, str)
    def update_server_list(self, name, ip, username):
        data = self.get_data_json()
        data['server_list'][name] = {"address": ip, "username": username}
        self.write_file(data)
        self.server_state_model.append({"name": name, "state": 0})
        self._set_server_state_model(self.server_state_model)
    
    # -- Delete server -- #
    @Slot(str)
    def delete_server(self, name):
        data = self.get_data_json()
        data['server_list'].pop(name)
        self.write_file(data)
        self.server_state_model = [item for item in self.server_state_model if item['name'] != name]
        self._set_server_state_model(self.server_state_model)

    
    # -- initial server state model: state = 0 -- #
    # -- run when init backend object -- #
    def get_initial_server_state(self):
        data = self.get_data_json()
        server_lst = data["server_list"]
        state_lst = []
        for ele in server_lst:
            state_lst.append({"name": ele, "state": 0})
        return state_lst

    # --- server list and state after ping -- #
    @Slot()
    def pingMultiHosts(self):
        data = self.get_data_json()
        print("data multi host: ", data)
        state_lst = utils.get_state_lst(data)
        print("state after ping multi: ", state_lst)
        self._set_server_state_model(state_lst)
        # print(self.server_state_model)
    
    # --- set private key file path -- #
    @Slot(str)
    def updateFilePath(self, data_input):
        data = self.get_data_json()
        if data_input != "": 
            data["file_path"] = data_input
        self.write_file(data)
    
    # --- set username of host -- #
    @Slot(str, str)
    def updateUsername(self, server_name, user):
        data = self.get_data_json()
        if user != "":
            data["server_list"][server_name]["username"] = user
        self.write_file(data)

    # -- saving server which is having ssh connection -- #
    def ssh_saving_tracking(self, serverName):
        self.ssh_tracker.append(serverName)


    # ---- Switch to current ssh client -- #
    @Slot(str)
    def switchClient(self, serverName):
        self.client = self.connection_lst[serverName]
        print(f'Switch to {serverName} client')

    # ---- connect ssh with information --- #
    @Slot(str, str, result='bool')
    def hasSSH(self, server_name, passphrase):
        data = self.get_data_json()
        hostname = data["server_list"][server_name]["address"]
        username = data["server_list"][server_name]["username"]
        file_private_key = data["file_path"]
        client = utils.client_init(hostname, username, file_private_key, self.passwd)
        if client != False:
            self.client = client
            self.connection_lst[server_name] = client   # -- Add new client into list connection
            
            # -- Download file to remote host -- #
            local_file_path = 'A://HoangAnh//local-server-tool//find_largest_dir.sh'
            remote_file_path = 'find_largest_dir.sh'

            with open(local_file_path, 'rb') as local_file:
                sftp = client.open_sftp()
                sftp.putfo(local_file, remote_file_path)
                sftp.close()
            # -------------- Done ----------------- #
            
            for i in range(len(self.server_state_model)):
                if self.server_state_model[i]["name"] == server_name:
                    #print(self.server_state_model[i]["name"])
                    self.server_state_model[i]["state"] = 2
                    #print(self.server_state_model)
                    self._set_server_state_model(self.server_state_model)
                    return True
        return False
    
    # -- checking ssh connection using socket -- #
    @Slot(str, result='bool')
    def check_ssh_connection(self, serverName):
        data = self.get_data_json()
        hostname = data["server_list"][serverName]["address"]
        sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        try:
            sock.settimeout(2)  # Set a timeout value in seconds
            sock.connect((hostname, 22))
            return True  # SSH connection is available
        except (socket.timeout, ConnectionRefusedError):
            return False  # SSH connection is not available
        finally:
            sock.close()

    # -- Save passphrase -- #
    # -- trigger after clicking apply passphrase -- #
    @Slot(str)
    def save_passphrase(self, passwd):
        self.passwd = passwd
        #print(self.passwd)

    # -- Adding server which is having ssh connection -- #
    def check_connection(self):
        data = self.get_data_json()
        # for server in self.server_state_model:
        #     if server['state'] == 2:
        #         server_name = server['name']
        #         hostname = data["server_list"][server_name]["address"]
        #         passwd = self.passwd
        #         if self.check_ssh_connection(serverName) or self.client is None:
        #             print(server['state'])


        for i in range(len(self.server_state_model)):
            if self.server_state_model[i]["state"] == 2:
                serverName = self.server_state_model[i]["name"]
                hostname = data["server_list"][serverName]["address"]
                passwd = self.passwd
                if self.check_ssh_connection(serverName) or self.client == False:
                    print(self.server_state_model[i]['state'])
                else:
                    self.ssh_saving_tracking(serverName) # -- Keep track ssh connection -- #
                    if utils.ping_host(hostname):
                        self.server_state_model[i]['state'] = 1
                        print(self.server_state_model[i]['state'])
                    else:
                        self.server_state_model[i]['state'] = 0
                        print(self.server_state_model[i]['state'])
        if len(self.ssh_tracker) == 0: pass
        else:
            for j in range(len(self.ssh_tracker)):
                for i in range(len(self.server_state_model)):
                    serverName = self.server_state_model[i]["name"]
                    hostname = data["server_list"][serverName]["address"]
                    if self.server_state_model[i]["name"] == self.ssh_tracker[j]:
                        if self.server_state_model[i]["state"] != 2:
                            if utils.ping_host(hostname):
                                self.server_state_model[i]['state'] = 1
                                print(self.server_state_model[i]['state'])
                            else:
                                self.server_state_model[i]['state'] = 0
                                print(self.server_state_model[i]['state'])
                        else: self.ssh_tracker.pop(j)
            self._set_server_state_model(self.server_state_model)
    
    # -- manually auto checking current ssh connection -- #
    # -- trigger after clicking connect ssh -- #
    @Slot()
    def manually_check_connection(self):
        def recursive_check():
            # self.checkout_connection()
            print("Checking...")
            #print(self.checkout_connection())
            self.check_connection()
            time.sleep(100)
            checkingTimer = threading.Thread(target=recursive_check, daemon=True)
            checkingTimer.start()

        timerControl = threading.Thread(target=recursive_check, daemon=True)
        timerControl.start()

        print("Let wait for manually Checking State...")

    @Slot()
    def close_all_connection(self):
        for ele in self.connection_lst:
            self.connection_lst[ele].close()
        self.connection_lst.clear()
        print("Close All Connections")

    # --- Changing "state" in model when manually checking connection -------------- #
    on_update_server_state_model = Signal()

    def _get_server_state_model(self):
        return self.server_state_model

    def _set_server_state_model(self, model):
        self.server_state_model = model
        self.on_update_server_state_model.emit()

    @Property('QVariant', _get_server_state_model, _set_server_state_model, notify=on_update_server_state_model)
    def stateColor(self):
        return self.server_state_model
    # ------- End of Signaling Color State ----------- #


    """ ----------------------------------------
    ----- Code for interacting with server -----
    ---------------------------------------------"""
    
    # -- This function is not used -- #
    # -- Intended to be used as custom input of command line -- # 
    @Slot(str, result='QString')
    def result_of_command(self, action):
        # if action != 'check-disk-filter':
        stdin, stdout, stderr = self.client.exec_command(self.action_command[action])
        if stderr != None:
            print(stderr.read().decode())
        return stdout.read().decode()


    """--------------- DISK MANAGEMENT --------------------
    --------------------------------------------------------"""

    # -- Handle data of disk management -- #
    @Slot(result='QVariant')
    def disk_data_model(self):
        stdin, stdout, stderr = self.client.exec_command(self.action_command['check-disk'])
        txt_data = stdout.read().decode()
        # rows = txt_data.split('\n')
        # disk_data = [list(row.split()) for row in rows]
        # disk_data_model = []

        # disk_data[0] = disk_data[0][:-1]
        # for i in range(len(disk_data)):
        #     if len(disk_data[i]) == 0: disk_data.pop(i)
            
        # for i in range(1, len(disk_data)):
        #     dict_tmp = {}
        #     for j in range(len(disk_data[0])):
        #         dict_tmp[disk_data[0][j]] = disk_data[i][j]
        #     disk_data_model.append(dict_tmp)
        disk_data_model = utils.handle_data_disk(txt_data)
        return disk_data_model

    # -- Handle data filter of disk management -- #
    @Slot(result='QVariant')
    def disk_data_filter_model(self):
        stdin, stdout, stderr = self.client.exec_command(self.action_command['check-disk-filter'])
        txt_data = stdout.read().decode()
        # rows = txt_data.split('\n')
        # disk_data = [list(row.split()) for row in rows]
        # disk_data_filter_model = []

        # disk_data[0] = disk_data[0][:-1]
        # for i in range(len(disk_data)):
        #     if len(disk_data[i]) == 0: disk_data.pop(i)
            
        # for i in range(1, len(disk_data)):
        #     dict_tmp = {}
        #     for j in range(len(disk_data[0])):
        #         dict_tmp[disk_data[0][j]] = disk_data[i][j]
        #     disk_data_filter_model.append(dict_tmp)
        disk_data_filter_model = utils.handle_data_disk(txt_data)
        return disk_data_filter_model 
    
    # -- get disk data from remote using BASH FILE -- #
    # -- currently in use -- #
    @Slot(str)
    def handle_disk_data(self, disk_path):
        # event = threading.Event()
        data = None
        # -- Get information of disk and return -- #
        def get_dir_storage():
            nonlocal data
            print("Start get remote data")
            command = f"bash find_largest_dir.sh {disk_path}"
            stdin, stdout, stderr = self.client.exec_command(command)
            stdout.channel.setblocking(True)
            data = stdout.read().decode()
            stdout.channel.setblocking(False)

            print("Received data !")
            print("Change to 1000ms")
            self._set_interval(1000)        # -- set interval of Timer -- #

            lst_data = data.split('\n')[:-1]
            lst_ele = [row.split() for row in lst_data]     # [['/A/B', '0'], ['/A/B/C', '26183648'], ['/A/B/C/D', '23638580']]
            
            for i in range(1, len(lst_ele)):
                lst_ele[i][0] = lst_ele[i][0].rsplit('/', 1)[-1:]    # [['/A/B', '0'], [['/A/B', 'C'], '26183648'], [['/A/B/C', 'D'], '23638580']]

            new_model = [{'name': lst_ele[0][0], 'size': utils.change_format(lst_ele[0][1])}]
            for i in range(1, len(lst_ele)):
                new_model.append({'name': lst_ele[i][0][0], 'size': utils.change_format(lst_ele[i][1])})
            
            self._set_largest_dir_model(new_model)
            print("Change to 10ms")
            self._set_interval(10)

        new_thread = threading.Thread(target=get_dir_storage, daemon=True)
        print("Initial new thread...")
        # self._set_interval(1500) 
        new_thread.start()

    """ # -- Remove file with pattern 'file' or 'link' -- #
    # -- This function is used with "Tree" command below -- # """
    """     def remove_files(self, json_data):
        if isinstance(json_data, list):
            return [self.remove_files(item) for item in json_data if item.get('type') not in ['file', 'link']]
        elif isinstance(json_data, dict):
            return {
                key: self.remove_files(value)
                for key, value in json_data.items()
                if not (key == 'contents' and json_data.get('type') in ['file', 'link'])
            }
        else:
            return json_data """

    """ # -- get disk data from remote using "tree" command -- #
        # -- Currently not in use -- #                          """
    """     @Slot(str)
    def handle_data(self, disk_path):
        event = threading.Event()
        def process_raw_data(raw_data):
            # Change interval faster
            print("Change to 1000ms")
            self._set_interval(1000)
            # Handle data
            json_rm = raw_data.replace('{"error": "error opening dir"}', '')
            json_data = json.loads(json_rm)
            filter_data = self.remove_files(json_data)

            # Get list model for largest directories
            root = filter_data[0]
            dir_model = [{'name': root['name'], 'size': self.change_format(root['size'])}]
            tmp_dict = root
            while True:
                if 'contents' not in tmp_dict or len(tmp_dict['contents']) == 0:
                    break
                tmp_dict = tmp_dict['contents'][0]
                dir_model.append({'name': tmp_dict['name'], 'size': self.change_format(tmp_dict['size'])})

            self._set_largest_dir_model(dir_model)
            print("Change to 10ms")
            self._set_interval(10)

        def get_data_remote():
            command = f"tree {disk_path} -a -s --du --noreport -I 'lost+found' -J --sort=size"
            stdin, stdout, stderr = self.client.exec_command(command)
            raw_data = stdout.read().decode()
            event.set()
            # Schedule the process_raw_data function to run after a delay
            sub2 = threading.Thread(target=process_raw_data, args=(raw_data,))
            sub2.start()
            event.wait()

        # Start the subthread to fetch the data
        sub1 = threading.Thread(target=get_data_remote, daemon=True)
        sub1.start()

        # Continue main thread execution without waiting for subthread
        # Perform other tasks here
        print("Continue main thread execution...") """
    

    # -- Reset model -- #
    @Slot()
    def reset_model_interval(self):
        self._set_interval(3000)
        self._set_largest_dir_model([])

    # ---------------------------------------- #
    # signal for use in setting interval Timer #
    # ---------------------------------------- #
    on_update_interval = Signal()

    def _get_interval(self):
        return self.interval

    def _set_interval(self, interval):
        self.interval = interval
        self.on_update_interval.emit()

    @Property(int, _get_interval, _set_interval, notify=on_update_interval)
    def interval_value(self):
        return self.interval
    # --------------------------------------#

    ## ------------------------------------ ##
    ## signal for largest directories model ##
    ## ------------------------------------ ##
    on_update_largest_dir_model = Signal()

    def _get_largest_dir_model(self):
        return self.largest_dir_model

    def _set_largest_dir_model(self, model):
        self.largest_dir_model = model
        self.on_update_largest_dir_model.emit()

    @Property('QVariantList', _get_largest_dir_model, _set_largest_dir_model, notify=on_update_largest_dir_model)
    def largest_model_value(self):
        return self.largest_dir_model
    # --------------------------------------#
    

    """--------------- WHO MANAGEMENT --------------------
    --------------------------------------------------------"""

    # -- Handle data of who checking -- #
    @Slot(result='QVariant')
    def who_data_model(self):
        stdin, stdout, stderr = self.client.exec_command(self.action_command['check-who-connecting'])
        txt_data = stdout.read().decode()
        # rows = txt_data.split('\n')
        # who_data = [list(row.split()) for row in rows]
        # who_data_model = []

        # who_data = who_data[:-1]
        # for i in range(len(who_data)):
        #     who_data[i][6] = who_data[i][6][1:-1]
        #     who_data[i][2] = who_data[i][2] + ' ' + who_data[i][3]
        #     who_data[i].pop(3)
        #     who_data[i].pop(4)
        
        # header = ['Username', 'Terminal No', 'Login Time', 'Activity', 'IP address']
        # for i in range(len(who_data)):
        #     dict_tmp = {}
        #     for j in range(len(header)):
        #         dict_tmp[header[j]] = who_data[i][j]
        #     who_data_model.append(dict_tmp)
        
        who_data_model = utils.handle_data_who(txt_data)
        return who_data_model

    # -- Get port of server from config file -- #
    def get_service_port(self, serverName):
        stdin, stdout, stderr = self.client.exec_command(self.action_command['check-https-port'])
        json_data = stdout.read().decode()
        # data = json.loads(json_data)

        # hostname = serverName
        # port_check = None

        # for i in range(len(data["server_configs"])):
        #     if data["server_configs"][i]["hostname"] == hostname:
        #         port_check = data["server_configs"][i]["https_port"]
        #         break
        port_check = utils.get_service(json_data, serverName)
        return port_check

    # -- Checking port connection -- #
    @Slot(str, result='QVariantList')
    def check_https_port_state(self, serverName):
        # get ip from serverName
        setting_data = self.get_data_json()
        address = setting_data["server_list"][serverName]["address"]
        https_port = self.get_service_port(serverName)

        stdin, stdout, stderr = self.client.exec_command(f'nc -w 5 -zv {address} {https_port} &>/dev/null && echo "Open" || echo "Closed"')
        result = stdout.read().decode()
        # print(result)
        # print(len(result))
        return [result, https_port]

    # -- get_state_service -- #
    def get_state_service(self):
        stdin, stdout, stderr = self.client.exec_command('systemctl --user is-active akselos-dev--scrbe_server')
        scrbe_state = stdout.read().decode()
        stdin, stdout, stderr = self.client.exec_command('systemctl --user is-active akselos-dev--dashboard')
        dashb_state = stdout.read().decode()
        #print(scrbe_state, dashb_state)
        return [scrbe_state, dashb_state]
    
    # -- Handle data of hosting service checking -- #
    @Slot(result='QVariant')
    def host_service_data_model(self):
        state = self.get_state_service()
        host_service = [
            {'Service': 'scrbe server', 'State': state[0]},
            {'Service': 'akselos dashboard', 'State': state[1]}
        ]
        return host_service

    # -- Get detail status of hosting service -- #
    @Slot(result='QVariantList')
    def get_detail_hosting_service(self):
        stdin, stdout, stderr = self.client.exec_command('systemctl --user status akselos-dev--scrbe_server')
        scrbe_detail = stdout.read().decode()
        stdin, stdout, stderr = self.client.exec_command('systemctl --user status akselos-dev--dashboard')
        dashb_detail = stdout.read().decode()
        return [scrbe_detail, dashb_detail]
    

    """------------------------------------------ JOB MANAGEMENT --------------------------------------
    ------------------------------------------------------------------------------------------------"""
    # -- Get List of user -- #
    def get_user_list(self):
        cmd = f'mysql -e "use UserDatabase; SELECT distinct Id, UserName from User ORDER BY Id ASC;"'
        stdin, stdout, stderr = self.client.exec_command(cmd)
        output = stdout.read().decode()
        lst = output.split('\n')[:-1]
        data = [row.split() for row in lst]
        user_dict = {}
        for i in range(1, len(data)):
            user_dict[data[i][0]] = data[i][1]
        
        return user_dict

    
    
    # -- Get username from PsId in Database or JobId in Slurm -- #
    def get_info_db(self, stime, etime):
        stime_dt = datetime.datetime.strptime(utils.format_time(stime), '%Y-%m-%d %H:%M')
        stime_dt -= datetime.timedelta(minutes=1)
        etime_dt = datetime.datetime.strptime(utils.format_time(etime), '%Y-%m-%d %H:%M')
        etime_dt += datetime.timedelta(minutes=1)
        print(stime_dt, etime_dt)
        cmd = f'mysql -e "use JobDatabase; select PsId, UserId from Job where StartTime >= \'{stime}\' AND FinishTime <= \'{etime}\';"'
        stdin, stdout, stderr = self.client.exec_command(cmd)
        stdout.channel.setblocking(True)
        op = stdout.read().decode()
        stdout.channel.setblocking(False)
        # print(op)

        lst = op.split('\n')[:-1]
        data = [row.split() for row in lst]
        
        psid_dic = {}
        for i in range(1, len(data)):
            psid_dic[data[i][0]] = data[i][1]
        
        # print(psid_dic)
        return psid_dic

    # -- Use for table view -- #
    def get_job_state(self, stime, etime):
        # curr = datetime.datetime.now()
        # two = curr - datetime.timedelta(hours=time)
        # start_time = two.strftime('%H:%M:%S')
        # print(start_time)
        #time = '2023-07-12T09:00'
        cmd = f'sacct -S {stime} -E {etime} -X -o jobid,jobname,user,start,end,state,ncpu'
        stdin, stdout, stderr = self.client.exec_command(cmd)
        output = stdout.read().decode()
        psid_dict = self.get_info_db(stime, etime)
        user_dict = self.get_user_list()

        # data1 = output.split('\n')
        # data1.pop(1)
        # data2 = [row.split() for row in data1][:-1]
        # for i in range(1, len(data2)):
        #     range_jobname = len(data2[i]) - 5
        #     data2[i][1] = ' '.join([data2[i][j] for j in range(1,range_jobname)])   # -- Concat jobname
        #     for j in range(2, range_jobname):       # -- remove duplicate jobname
        #         data2[i].pop(2)
        #     data2[i][3] = data2[i][3].replace('T',' ')      # -- Split start time
        #     data2[i][4] = data2[i][4].replace('T',' ')      # -- Split end time
        # # print(data2)
        
        # job_model = []
        # for i in range(1, len(data2)):
        #     dic = {}
        #     for j in range(len(data2[i])):
        #         dic[data2[0][j]] = data2[i][j]
        #     job_model.append(dic)
        
        
        # for i in range(len(job_model)):
        #     psid = job_model[i]['JobID']
        #     userid = ''
        #     if psid in psid_dict:
        #         userid = psid_dict[job_model[i]['JobID']]
        #     if userid in user_dict:
        #         username = user_dict[userid]
        #         job_model[i]['User'] = username
        #     else: job_model[i]['User'] = 'Unknown'
        job_model = utils.handle_job_data(0, output, psid_dict, user_dict)
        # print(job_model)
        return job_model
    
    # -- Use for chart view -- #
    # -- No database query for seeking usernam -- #
    def get_job_state_2(self, stime, etime):
        cmd = f'sacct -S {stime} -E {etime} -X -o jobid,jobname,user,start,end,state,ncpu'
        stdin, stdout, stderr = self.client.exec_command(cmd)
        output = stdout.read().decode()

        # data1 = output.split('\n')
        # data1.pop(1)
        # data2 = [row.split() for row in data1][:-1]
        # for i in range(1, len(data2)):
        #     range_jobname = len(data2[i]) - 5
        #     data2[i][1] = ' '.join([data2[i][j] for j in range(1,range_jobname)])   # -- Concatenate jobname
        #     for j in range(2, range_jobname):       # -- remove duplicate jobname
        #         data2[i].pop(2)
        #     data2[i][3] = data2[i][3].replace('T',' ')      # -- Split start time
        #     data2[i][4] = data2[i][4].replace('T',' ')      # -- Split end time
        # # print(data2)

        # job_model = []
        # for i in range(1, len(data2)):
        #     dic = {}
        #     for j in range(len(data2[i])):
        #         dic[data2[0][j]] = data2[i][j]
        #     job_model.append(dic)

        job_model = utils.handle_job_data(1, output)
        return job_model
    
    # -- use for finished jobs
    @Slot(str, str, result='QVariantList')
    def finish_job_model(self, stime, etime):
        model = self.get_job_state(stime, etime)
        i = 0
        while i < len(model):
            if model[i]['State'] == 'RUNNING': model.pop(i)
            else: i += 1

        return model

    # -- use for running jobs
    @Slot(str, str, result='QVariantList')
    def running_job_model(self, stime, etime):
        model = self.get_job_state(stime, etime)
        i = 0
        while i < len(model):
            if model[i]['State'] != 'RUNNING': model.pop(i)
            else: i += 1

        # print(model)
        return model
    
    # -- Get more info of job -- #
    @Slot(str, result='QVariantList')
    def get_detail_job(self, jobid):
        cmd = f'mysql -e "use JobDatabase;  select Id,PsId, UserId,JobType,Collection,Status from Job where PsId = {jobid} order by id desc LIMIT 1;"'
        stdin, stdout, stderr = self.client.exec_command(cmd)
        output = stdout.read().decode()
        # rows = output.split('\n')
        # data = [row.split() for row in rows][:-1]
        # # print(data)
        # dic = {}
        # for i in range(len(data[0])):
        #     dic[data[0][i]] = data[1][i]
        # for key in dic:
        #     if key == 'Status':
        #         if dic[key] == 'S': dic[key] = 'Suspended'
        #         elif dic[key] == 'F': dic[key] = 'Failed'
        #         else: dic[key] = 'Running'
        model = utils.get_detail(output)
        return model
    
    # ------------------------------------------------------- #
    # --------- LINE CHART VIEW FOR JOB FREQUENCY ----------- #
    # ------------------------------------------------------- #
    on_update_job_chart_model = Signal()

    def _get_job_chart_model(self):
        return self.job_chart_model

    def _set_job_chart_model(self, model):
        self.job_chart_model = model
        self.on_update_job_chart_model.emit()

    @Property('QVariantList', _get_job_chart_model, _set_job_chart_model, notify=on_update_job_chart_model)
    def lineChartJobData(self):
        return self.job_chart_model
    
    # -- chart data in one day (currently date) -- #
    @Slot(str)
    def get_line_chart_data_hour(self, stime):
        # curr_date = stime.split('T')[0]
        # date_obj = datetime.datetime.strptime(curr_date, '%Y-%m-%d')
        # next_day = date_obj + datetime.timedelta(days=1)
        # end_next_day = next_day.strftime('%Y-%m-%d')
        
        # job_model = self.get_job_state_2(curr_date, end_next_day)
        # # print(curr_date)
        # # print(end_next_day)

        # job = {i: 0 for i in range(24)}
        # ncpu = {i: 0 for i in range(24)}

        # for i in range(len(job_model)):
        #     date = job_model[i]['Start'].split()[0]
        #     if date == curr_date:
        #         time = job_model[i]['Start'].split()[1]
        #         hour = time.split(':')[0]
        #         hour = int(hour)
        #         job[hour] += 1
        #         ncpu[hour] += int(job_model[i]['NCPUS'])

        # point_job_lst = [[i, job[i]] for i in job]
        # point_ncpu_lst = [[i, ncpu[i]] for i in ncpu]
        
        # # print(job)
        # # print(ncpu)
        # min_y = 0
        # max_y = ncpu[0]
        # for ele in ncpu:
        #     if ncpu[ele] >= max_y:
        #         max_y = ncpu[ele]
        
        # range_x = [0, 23]
        # range_y = [min_y, max_y]
        # chart_model = [[point_job_lst, point_ncpu_lst], range_x, range_y, ['Hour [h]', 'Quantity [job / cpu]']]
        # print(chart_model)
        chart_model = utils.handle_line_data('hour', stime, self.get_job_state_2)
        self._set_job_chart_model(chart_model)

    # -- chart data in one month (currently month) -- #
    @Slot(str)
    def get_line_chart_data_day(self, stime):
        # curr_date = stime.split('T')[0]
        # parsed_date = datetime.datetime.strptime(curr_date, '%Y-%m-%d')
        # start_time = parsed_date.strftime('%Y-%m-01')
        # curr_year_month = parsed_date.strftime('%Y-%m')
        
        # end_time = parsed_date + datetime.timedelta(days=31)
        # end_time = end_time.strftime('%Y-%m-01')

        # # print(start_time)
        # # print(end_time)

        # job_model = self.get_job_state_2(start_time, end_time)
        # # print("Job model: ", job_model)

        # curr_month = curr_date.split('-')[1]

        # job_m = {i: 0 for i in range(32)} if curr_month in ['01','03','05','07','08','10','12'] else {i: 0 for i in range(31)}
        # ncpu_m = {i: 0 for i in range(32)} if curr_month in ['01','03','05','07','08','10','12'] else {i: 0 for i in range(31)}

        # for i in range(len(job_model)):
        #     date = job_model[i]['Start'].split()[0]
        #     day = date.split('-')[2]
        #     parsed_date = datetime.datetime.strptime(date, '%Y-%m-%d')
        #     year_month = parsed_date.strftime('%Y-%m')
        #     if year_month == curr_year_month:
        #         day = int(day)
        #         job_m[day] += 1
        #         ncpu_m[day] += int(job_model[i]['NCPUS'])

        # point_job_lst = [[i, job_m[i]] for i in job_m]
        # point_ncpu_lst = [[i, ncpu_m[i]] for i in ncpu_m]

        # min_y = 0
        # max_y = ncpu_m[0]
        # for ele in ncpu_m:
        #     if ncpu_m[ele] >= max_y:
        #         max_y = ncpu_m[ele]
        
        # range_x = [0, 31]
        # range_y = [min_y, max_y]
        # chart_model = [[point_job_lst, point_ncpu_lst], range_x, range_y, ['Day [d]', 'Quantity [job / cpu]']]
        # print(chart_model)
        chart_model = utils.handle_line_data('day', stime, self.get_job_state_2)
        self._set_job_chart_model(chart_model)
    

    # ------------------------------------------------------- #
    # -------- PIE CHART VIEW BY USERS FOR JOB FREQUENCY ---- #
    # ------------------------------------------------------- #

    # -- Property of Job and NCPU 
    on_update_job_pie_chart_model = Signal()
    on_update_ncpu_pie_chart_model = Signal()

    def _get_job_pie_chart_model(self):
        return self.job_pie_chart_model

    def _get_ncpu_pie_chart_model(self):
        return self.ncpu_pie_chart_model

    def _set_job_pie_chart_model(self, model):
        self.job_pie_chart_model = model
        self.on_update_job_pie_chart_model.emit()
    
    def _set_ncpu_pie_chart_model(self, model):
        self.ncpu_pie_chart_model = model
        self.on_update_ncpu_pie_chart_model.emit()

    @Property('QVariantList', _get_job_pie_chart_model, _set_job_pie_chart_model, notify=on_update_job_pie_chart_model)
    def pieChartJobData(self):
        return self.job_pie_chart_model

    @Property('QVariantList', _get_ncpu_pie_chart_model, _set_ncpu_pie_chart_model, notify=on_update_ncpu_pie_chart_model)
    def pieChartNCPUData(self):
        return self.ncpu_pie_chart_model
    
    # -- chart data following user in one week (since start time) -- #
    @Slot(str)
    def get_pie_chart_by_user(self, stime):
        # # curr_date = stime.split('T')[0]
        # date_obj = datetime.datetime.strptime(stime, '%Y-%m-%dT%H:%M')
        # one_week_obj = date_obj - datetime.timedelta(weeks=1)
        # one_week_before = one_week_obj.strftime('%Y-%m-%dT%H:%M')
        # # print(one_week_before)
        # # print(stime)
        # job_model = self.get_job_state(one_week_before, stime)
        # # print(end_next_day)

        # dict_data_job = {}
        # dict_data_ncpu = {}
        # for i in range(len(job_model)):
        #     if job_model[i]['User'] in dict_data_job and job_model[i]['User'] in dict_data_ncpu:
        #         dict_data_job[job_model[i]['User']] += 1
        #         dict_data_ncpu[job_model[i]['User']] += int(job_model[i]['NCPUS'])
        #     else:
        #         dict_data_job[job_model[i]['User']] = 1
        #         dict_data_ncpu[job_model[i]['User']] = int(job_model[i]['NCPUS'])

        # lst_data_job = []
        # lst_data_ncpu = []
        # for ele in dict_data_job:
        #     lst_data_job.append([ele, dict_data_job[ele]])
        #     lst_data_ncpu.append([ele, dict_data_ncpu[ele]])

        # chart_model_job = [lst_data_job, 'Job']
        # chart_model_ncpu = [lst_data_ncpu, 'NCPU']
        model = utils.handle_pie_data(stime, self.get_job_state)
        chart_model_job = model[0]
        chart_model_ncpu = model[1]
        self._set_job_pie_chart_model(chart_model_job)
        self._set_ncpu_pie_chart_model(chart_model_ncpu)
    
    # ------- LineChart using interval ---- #
    on_update_interval_chart_model = Signal()

    def _get_interval_chart_model(self):
        return self.interval_chart_model

    def _set_interval_chart_model(self, model):
        self.interval_chart_model = model
        self.on_update_interval_chart_model.emit()

    @Property('QVariantList', _get_interval_chart_model, _set_interval_chart_model, notify=on_update_interval_chart_model)
    def lineChartIntervalData(self):
        return self.interval_chart_model

    @Slot(str, str, int)
    def get_interval_line_chart(self, stime, etime, interval):
        job_model = self.get_job_state_2(stime, etime)
        data_model = utils.get_data_model(job_model, stime, etime, interval)

        min_x = data_model[0][0][0]
        max_x = data_model[0][len(data_model[0])-1][0]

        min_y = 0
        max_y = 0
        for idx in range(len(data_model[1])):
            if data_model[1][idx][1] >= max_y:
                max_y = data_model[1][idx][1]

        model = [data_model, [min_x, max_x], [min_y, max_y]]
        self._set_interval_chart_model(model)