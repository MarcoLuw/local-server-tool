import QtQuick.Dialogs 1.3
import QtQuick 2.12
import QtQuick 2.15
import QtQuick 2.5
import QtQuick.Window 2.12
import QtQuick.Window 2.2
import QtQuick.Layouts 1.1
import QtQuick.Controls 1.1
import QtQuick.Controls 1.4
import QtQuick.Controls 1.5
import QtQuick.Controls 2.15
import QtQml.Models 2.15
import QtGraphicalEffects 1.15
import QtCharts 2.15

ChartView {
    id: pieChart
    title: ""
    antialiasing: true
    theme: ChartView.ChartThemeDark
    animationOptions : ChartView.AllAnimations

    PieSeries {
        name: "Job"
        id: userSeriresId
    }

    function changeLabel(slice, state) {
        slice.labelVisible = state
        slice.labelPosition = PieSlice.LabelInsideNormal
    }

    function illustratrate_pieChart(num) {
        pieChart.removeAllSeries()
        console.log("Num cua pie chart 2: ", num)
        var chart_data = (num === 0) ? backend.pieChartJobData : backend.pieChartNCPUData
        var pie_chart_data = chart_data[0]
        var pieChart_name = chart_data[1]
        var title = "<h2 style='text-transform: uppercase;'><b>" + pieChart_name + "</b></h2>"
        pieChart.title = title

        if (chart_data.length !== 0) {
            var name_of_pie = "<b>" + pieChart_name + "</b>"
            var series = pieChart.createSeries(ChartView.SeriesTypePie, name_of_pie)
            series.holeSize = 0.3
            for (var i = 0; i < pie_chart_data.length; i++) {
                var slice = series.append("<b>"+pie_chart_data[i][0]+" ("+pie_chart_data[i][1]+")"+"</b>", pie_chart_data[i][1])
                function do_func(s) {
                    s.hovered.connect(function(state) {
                    pieChart.changeLabel(s, state)
                    })
                }
                do_func(slice)
            }
        }
    }
}