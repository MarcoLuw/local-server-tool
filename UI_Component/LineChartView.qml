import QtQuick.Dialogs 1.3
import QtQuick 2.12
import QtQuick 2.15
import QtQuick 2.5
import QtQuick.Window 2.12
import QtQuick.Window 2.2
import QtQuick.Layouts 1.1
import QtQuick.Controls 1.1
import QtQuick.Controls 1.4
import QtQuick.Controls 1.5
import QtQuick.Controls 2.15
import QtQml.Models 2.15
import QtGraphicalEffects 1.15
import QtCharts 2.15

ChartView {
    id: lineChart
    title: "<h3 style='text-transform: uppercase;'><b>Job Frequency</b></h3>"
    anchors.fill: parent
    antialiasing: true
    theme: ChartView.ChartThemeDark
    animationOptions : ChartView.AllAnimations

    axes: [
        ValueAxis {
            id: xLineAxis
            // min: 0
            // max: 23
            //tickCount: 24
            titleText: ""
        },
        ValueAxis {
            id: yLineAxis
            // min: 0
            //max: 23
            // tickCount: 5
            titleText: ""
        }
    ]
    LineSeries {
        name: ""
        id: jobSeriesId
        axisX: xLineAxis
        axisY: yLineAxis
    }

    function plot_soil_illus(interval) {
        lineChart.removeAllSeries()
        var chart_data = backend.lineChartJobData
        var line_chart_data = chart_data[0]
        var min_x = chart_data[1][0]
        var max_x = chart_data[1][1]
        var min_y = chart_data[2][0]
        var max_y = chart_data[2][1]
        var title_axis = chart_data[3]

        xLineAxis.min = min_x
        xLineAxis.max = max_x
        yLineAxis.min = min_y
        yLineAxis.max = max_y

        // console.log("Chart data: ", chart_data)
        // console.log("Line chart data: ", Object.keys(line_chart_data[0]))
        xLineAxis.tickCount = interval
        
        xLineAxis.titleText = title_axis[0]
        yLineAxis.titleText = title_axis[1]
        
        if (line_chart_data.length !== 0) {
            var lineName = ['Number of Jobs', 'Number of NCPUs']
            for (var j = 0; j < line_chart_data.length; j++) {
                var name_of_line = "<b>" + lineName[j] + "</b>"
                var series = lineChart.createSeries(ChartView.SeriesTypeLine, name_of_line, xLineAxis, yLineAxis)
                var each_line = line_chart_data[j]
                for (var i = 0; i < each_line.length; i++) {
                    series.append(each_line[i][0], each_line[i][1])
                }
            }
        }
    }
}