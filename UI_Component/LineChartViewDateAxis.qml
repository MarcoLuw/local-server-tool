import QtQuick.Dialogs 1.3
import QtQuick 2.12
import QtQuick 2.15
import QtQuick 2.5
import QtQuick.Window 2.12
import QtQuick.Window 2.2
import QtQuick.Layouts 1.1
import QtQuick.Controls 1.1
import QtQuick.Controls 1.4
import QtQuick.Controls 1.5
import QtQuick.Controls 2.15
import QtQml.Models 2.15
import QtGraphicalEffects 1.15
import QtCharts 2.15

ChartView {
    id: lineChart
    title: "<h3 style='text-transform: uppercase;'><b>Job Frequency</b></h3>"
    anchors.fill: parent
    antialiasing: true
    theme: ChartView.ChartThemeDark
    animationOptions : ChartView.AllAnimations

    axes: [
        DateTimeAxis {
            id: xLineAxis
            // min: 0
            // max: 23
            //tickCount: 24
            format: "&#160;&#160;&#160;&#160;&#160;hh:mm<br>yyyy/MM/dd" // Where &#160 - spacing character in HTML
        },
        ValueAxis {
            id: yLineAxis
            // min: 0
            //max: 23
            // tickCount: 5
            titleText: "Number"
        }
    ]
    LineSeries {
        name: ""
        id: jobSeriesId
        axisX: xLineAxis
        axisY: yLineAxis
    }

    function plot_soil_illus_interval(interval) {
        lineChart.removeAllSeries()
        var chart_data = backend.lineChartIntervalData
        var line_chart_data = chart_data[0]
        var min_x = chart_data[1][0]
        var max_x = chart_data[1][1]
        var min_y = chart_data[2][0]
        var max_y = chart_data[2][1]

        // console.log("==========================", min_x, max_x)
        // console.log("==========================", new Date(min_x * 1000), new Date(max_x * 1000))

        xLineAxis.min = new Date(min_x * 1000)
        xLineAxis.max = new Date(max_x * 1000)
        yLineAxis.min = min_y
        yLineAxis.max = max_y

        // console.log("Chart data: ", chart_data)
        // console.log("Line chart data: ", Object.keys(line_chart_data[0]))
        // console.log("file ", interval)
        xLineAxis.tickCount = interval + 1
        
        if (line_chart_data.length !== 0) {
            var lineName = ['Number of Jobs', 'Number of NCPUs']
            for (var j = 0; j < line_chart_data.length; j++) {
                var name_of_line = "<b>" + lineName[j] + "</b>"
                var series = lineChart.createSeries(ChartView.SeriesTypeLine, name_of_line, xLineAxis, yLineAxis)
                var each_line = line_chart_data[j]
                for (var i = 0; i < each_line.length; i++) {
                    // print(each_line[i][0], each_line[i][1])
                    var date = new Date(each_line[i][0] * 1000)
                    series.append(date, each_line[i][1])
                }
            }
        }
    }
}