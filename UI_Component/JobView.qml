import QtQuick.Dialogs 1.3
import QtQuick 2.12
import QtQuick 2.15
import QtQuick 2.5
import QtQuick.Window 2.12
import QtQuick.Window 2.2
import QtQuick.Layouts 1.1
import QtQuick.Controls 1.1
import QtQuick.Controls 1.4
import QtQuick.Controls 1.5
import QtQuick.Controls 2.15
import QtQml.Models 2.15
import QtGraphicalEffects 1.15

TableView {
    id: job_table
    Layout.fillWidth: true
    Layout.fillHeight: true
    rowDelegate: Rectangle {
        height: 30
    }

    itemDelegate: Label {
        text: styleData.value
        font.pixelSize: 18
        horizontalAlignment: Text.AlignHCenter
        color: {
            if (styleData.role === "State") {
                if (styleData.value === "FAILED")
                    return "#f21d1d"
                else if (styleData.value === "COMPLETED")
                    return "#00cf3b"
                else return "#004cff"
            }
        }
        font.weight: {
            if (styleData.role === "State")
                return Font.ExtraBold
        }
        background: MouseArea {
            id: mouseArea_jobstate
            anchors.fill: parent
            acceptedButtons: Qt.RightButton
            
            onClicked: {
                jobstate.popup()
            }
            Menu {
                id: jobstate
                MenuItem {
                    id: show_jobstate
                    text: "Show more job detail"
                    onTriggered: {
                        jobDetail_table.model = backend.get_detail_job(modelData.JobID)
                        jobDetailPopup.open()
                    }
                }
            }
        }
    }

    Component.onCompleted: {
        var headers = [
            {role: 'JobID', title: 'JobID'},
            {role: 'JobName', title: 'JobName'},
            {role: 'User', title: 'User'},
            {role: 'Start', title: 'Start Time'},
            {role: 'End', title: 'End Time'},
            {role: 'State', title: 'State'},
            {role: 'NCPUS', title: 'NCPUS'}
        ]

        for (var i = 0; i < headers.length; i++)
            job_table.addColumn(cmp.createObject(job_table, headers[i]))
    }

    Popup {
        id: jobDetailPopup
        dim: false
        width: parent.width
        height: 100
        anchors.centerIn: parent
        background: Rectangle {
            anchors.fill: parent
            color:  '#C2DEDC'
        }
        contentItem: TableView {
            id: jobDetail_table
            rowDelegate: Rectangle {
                height: 30
            }
            itemDelegate: Label {
                text: styleData.value
                font.pixelSize: 18
                horizontalAlignment: Text.AlignHCenter
            }
            onModelChanged: jobDetail_table.resizeColumnToContents()
            Component.onCompleted: {
                var headers = [
                    {role: 'Id', title: 'Id'},
                    {role: 'PsId', title: 'PsId'},
                    {role: 'UserId', title: 'UserId'},
                    {role: 'JobType', title: 'JobType'},
                    {role: 'Collection', title: 'Collection'},
                    {role: 'Status', title: 'Status'}
                ]

                for (var i = 0; i < headers.length; i++)
                    jobDetail_table.addColumn(cmp.createObject(jobDetail_table, headers[i]))
            }
        }
        closePolicy: Popup.CloseOnEscape | Popup.CloseOnPressOutside
    }
}