import sys
import os
import subprocess
import paramiko as pk
from PySide2.QtCore import QObject, QProcess, Signal, Slot, Property
import json
import threading
import socket
import time
from datetime import datetime, timedelta

# -- Ping to server with ip address --- #
def ping_host(hostname):
    result = subprocess.run(['ping', hostname, '-n', '2', '-w', '500'])
    if result.returncode == 0: 
        return True
    else: return False

# -- Get state list after ping -- #
def get_state_lst(data):
    server_lst = data["server_list"]
    state_lst = []
    for ele in server_lst:
        address = server_lst[ele]["address"]
        result = subprocess.run(
            ['ping', address, '-n', '2', '-w', '500'])
        state_lst.append({"name": ele, "state": 1 if result.returncode == 0 else 0})
    return state_lst


# ------ initial ssh client waiting for connect -- #
def client_init(hostname, username, file_path, passphrase):
    try:
        client = pk.SSHClient()
        client.load_system_host_keys()
        client.set_missing_host_key_policy(pk.AutoAddPolicy())
        client.connect(
            hostname=hostname, port=22, 
            username=username, key_filename=file_path, 
            passphrase=passphrase, timeout=3
            )
        return client
    except Exception:
        return None


""" ----------------------------- Disk function area ----------------------------- """
def handle_data_disk(txt_data):
    rows = txt_data.split('\n')
    disk_data = [list(row.split()) for row in rows]
    disk_data_model = []

    disk_data[0] = disk_data[0][:-1]
    for i in range(len(disk_data)):
        if len(disk_data[i]) == 0: disk_data.pop(i)
        
    for i in range(1, len(disk_data)):
        dict_tmp = {}
        for j in range(len(disk_data[0])):
            dict_tmp[disk_data[0][j]] = disk_data[i][j]
        disk_data_model.append(dict_tmp)
    
    return disk_data_model

# -- Change format of unit -- #
def change_format(kbyte_number):
    units = ['B', 'MB', 'GB', 'TB', 'PB']
    size = int(kbyte_number)
    for i in range(len(units)):
        if size < 1024 or i == len(units) - 1:
            break
        size /= 1024
    size = round(size, 2)
    return f'{size} {units[i]}'


""" ----------------------- Who function area -------------------------- """
def handle_data_who(txt_data):
    rows = txt_data.split('\n')
    who_data = [list(row.split()) for row in rows]
    who_data_model = []

    who_data = who_data[:-1]
    for i in range(len(who_data)):
        who_data[i][6] = who_data[i][6][1:-1]
        who_data[i][2] = who_data[i][2] + ' ' + who_data[i][3]
        who_data[i].pop(3)
        who_data[i].pop(4)
    
    header = ['Username', 'Terminal No', 'Login Time', 'Activity', 'IP address']
    for i in range(len(who_data)):
        dict_tmp = {}
        for j in range(len(header)):
            dict_tmp[header[j]] = who_data[i][j]
        who_data_model.append(dict_tmp)
    return who_data_model

def get_service(json_data, serverName):
    data = json.loads(json_data)

    hostname = serverName
    port_check = None

    for i in range(len(data["server_configs"])):
        if data["server_configs"][i]["hostname"] == hostname:
            port_check = data["server_configs"][i]["https_port"]
            break
    return port_check

""" ----------------------- Job function area -------------------------- """

""" --- Function for table view --- """
def handle_job_data(divide_num, txt_data, psid_dict=None, user_dict=None):
    data1 = txt_data.split('\n')
    data1.pop(1)
    data2 = [row.split() for row in data1][:-1]
    for i in range(1, len(data2)):
        range_jobname = len(data2[i]) - 5
        data2[i][1] = ' '.join([data2[i][j] for j in range(1,range_jobname)])   # -- Concatenate jobname
        for j in range(2, range_jobname):       # -- remove duplicate jobname
            data2[i].pop(2)
        data2[i][3] = data2[i][3].replace('T',' ')      # -- Split start time
        data2[i][4] = data2[i][4].replace('T',' ')      # -- Split end time
    # print(data2)

    job_model = []
    for i in range(1, len(data2)):
        dic = {}
        for j in range(len(data2[i])):
            dic[data2[0][j]] = data2[i][j]
        job_model.append(dic)

    if divide_num == 0:
        for i in range(len(job_model)):
            psid = job_model[i]['JobID']
            userid = ''
            if psid in psid_dict:
                userid = psid_dict[job_model[i]['JobID']]
            if userid in user_dict:
                username = user_dict[userid]
                job_model[i]['User'] = username
            else: job_model[i]['User'] = 'Unknown'
    return job_model

def get_detail(txt_data):
    rows = txt_data.split('\n')
    data = [row.split() for row in rows][:-1]
    # print(data)
    dic = {}
    for i in range(len(data[0])):
        dic[data[0][i]] = data[1][i]
    for key in dic:
        if key == 'Status':
            if dic[key] == 'S': dic[key] = 'Suspended'
            elif dic[key] == 'F': dic[key] = 'Failed'
            else: dic[key] = 'Running'
    return [dic]


""" --- Function for line chart --- """
# -- Function for handle data  -- #
def handle_line_data(divide_word, stime, get_job_state_2):
    if divide_word == 'hour':
        curr_date = stime.split('T')[0]
        date_obj = datetime.strptime(curr_date, '%Y-%m-%d')
        next_day = date_obj + timedelta(days=1)
        end_next_day = next_day.strftime('%Y-%m-%d')
        
        job_model = get_job_state_2(curr_date, end_next_day)

        job = {i: 0 for i in range(24)}
        ncpu = {i: 0 for i in range(24)}

        for i in range(len(job_model)):
            date = job_model[i]['Start'].split()[0]
            if date == curr_date:
                time = job_model[i]['Start'].split()[1]
                hour = time.split(':')[0]
                hour = int(hour)
                job[hour] += 1
                ncpu[hour] += int(job_model[i]['NCPUS'])

        point_job_lst = [[i, job[i]] for i in job]
        point_ncpu_lst = [[i, ncpu[i]] for i in ncpu]
        
        min_y = 0
        max_y = ncpu[0]
        for ele in ncpu:
            if ncpu[ele] >= max_y:
                max_y = ncpu[ele]
        
        range_x = [0, 23]
        range_y = [min_y, max_y]
        chart_model = [[point_job_lst, point_ncpu_lst], range_x, range_y, ['Hour [h]', 'Quantity [job / cpu]']]
        return chart_model
    else:
        curr_date = stime.split('T')[0]
        parsed_date = datetime.strptime(curr_date, '%Y-%m-%d')
        start_time = parsed_date.strftime('%Y-%m-01')
        curr_year_month = parsed_date.strftime('%Y-%m')
        
        end_time = parsed_date + timedelta(days=31)
        end_time = end_time.strftime('%Y-%m-01')

        job_model = get_job_state_2(start_time, end_time)

        curr_month = curr_date.split('-')[1]

        job_m = {i: 0 for i in range(32)} if curr_month in ['01','03','05','07','08','10','12'] else {i: 0 for i in range(31)}
        ncpu_m = {i: 0 for i in range(32)} if curr_month in ['01','03','05','07','08','10','12'] else {i: 0 for i in range(31)}

        for i in range(len(job_model)):
            date = job_model[i]['Start'].split()[0]
            day = date.split('-')[2]
            parsed_date = datetime.strptime(date, '%Y-%m-%d')
            year_month = parsed_date.strftime('%Y-%m')
            if year_month == curr_year_month:
                day = int(day)
                job_m[day] += 1
                ncpu_m[day] += int(job_model[i]['NCPUS'])

        point_job_lst = [[i, job_m[i]] for i in job_m]
        point_ncpu_lst = [[i, ncpu_m[i]] for i in ncpu_m]

        min_y = 0
        max_y = ncpu_m[0]
        for ele in ncpu_m:
            if ncpu_m[ele] >= max_y:
                max_y = ncpu_m[ele]
        
        range_x = [0, 31]
        range_y = [min_y, max_y]
        chart_model = [[point_job_lst, point_ncpu_lst], range_x, range_y, ['Day [d]', 'Quantity [job / cpu]']]
        return chart_model


""" --- Function for pie chart --- """
def handle_pie_data(stime, get_job_state):
    # curr_date = stime.split('T')[0]
    date_obj = datetime.strptime(stime, '%Y-%m-%dT%H:%M')
    one_week_obj = date_obj - timedelta(weeks=1)
    one_week_before = one_week_obj.strftime('%Y-%m-%dT%H:%M')
    # print(one_week_before)
    # print(stime)
    job_model = get_job_state(one_week_before, stime)
    # print(end_next_day)

    dict_data_job = {}
    dict_data_ncpu = {}
    for i in range(len(job_model)):
        if job_model[i]['User'] in dict_data_job and job_model[i]['User'] in dict_data_ncpu:
            dict_data_job[job_model[i]['User']] += 1
            dict_data_ncpu[job_model[i]['User']] += int(job_model[i]['NCPUS'])
        else:
            dict_data_job[job_model[i]['User']] = 1
            dict_data_ncpu[job_model[i]['User']] = int(job_model[i]['NCPUS'])

    lst_data_job = []
    lst_data_ncpu = []
    for ele in dict_data_job:
        lst_data_job.append([ele, dict_data_job[ele]])
        lst_data_ncpu.append([ele, dict_data_ncpu[ele]])

    chart_model_job = [lst_data_job, 'Job']
    chart_model_ncpu = [lst_data_ncpu, 'NCPU']
    return [chart_model_job, chart_model_ncpu]


""" --- Function for line chart using interval --- """
# -- format time used for database -- #
def format_time(time):
    date_obj = datetime.strptime(time, '%Y-%m-%dT%H:%M')
    formatted_time = date_obj.strftime('%Y-%m-%d %H:%M')
    return formatted_time

# -- change datetime to timestamp -- #
def change_to_timestamp(time):
    time_obj = datetime.strptime(time, '%Y-%m-%d %H:%M')
    timestamp = time_obj.timestamp()
    return timestamp

# --- Get time list by interval --- #
def get_time_list(starttime, endtime, num_interval):
    # Convert starttime and endtime strings to datetime objects
    starttime = datetime.strptime(starttime, '%Y-%m-%d %H:%M')
    endtime = datetime.strptime(endtime, '%Y-%m-%d %H:%M')

    # Calculate the total time duration between starttime and endtime
    total_duration = (endtime - starttime).total_seconds()

    # Calculate the time step for each interval
    time_step = total_duration / num_interval

    # Calculate the time at each interval
    time_list = []
    current_time = starttime
    for i in range(num_interval):
        time_list.append(current_time.strftime('%Y-%m-%d %H:%M'))           # -- Change to timestamp -- #
        current_time += timedelta(seconds=time_step)

    time_list.append(endtime.strftime('%Y-%m-%d %H:%M'))
    return time_list

# -- Compare time -- #
def is_inside_interval(time1, time2, time3):
    # -- Time 1: value at each interval
    # -- Time 2: start time value of element in model
    # -- Time 3: end time value of element in model
    datetime_obj = datetime.strptime(time2, '%Y-%m-%d %H:%M:%S')
    starttime = datetime_obj.strftime('%Y-%m-%d %H:%M')
    datetime_obj = datetime.strptime(time3, '%Y-%m-%d %H:%M:%S')
    endtime = datetime_obj.strftime('%Y-%m-%d %H:%M')

    # Convert time strings to datetime objects
    datetime1 = datetime.strptime(time1, '%Y-%m-%d %H:%M')
    start = datetime.strptime(starttime, '%Y-%m-%d %H:%M')
    end = datetime.strptime(endtime, '%Y-%m-%d %H:%M')

    # Compare the datetime objects
    if start <= datetime1 <= end:
        return True
    else: return False

# -- get chart data model by interval -- #
def get_data_model(job_model, stime, etime, interval):
    stime = format_time(stime)          # -- remove middle "T"
    etime = format_time(etime)          # -- remove middle "T"
    time_lst = get_time_list(stime, etime, interval)
    # print(interval)
    # print(time_lst)
    timestamp_lst = [change_to_timestamp(time_lst[idx]) for idx in range(len(time_lst))]
  
    model_for_job = [[timestamp_lst[idx], 0] for idx in range(len(time_lst))]
    model_for_ncpu = [[timestamp_lst[idx], 0] for idx in range(len(time_lst))]

    for job_idx in range(len(job_model)):
        if job_model[job_idx]['End'] != 'Unknown':
            for date_idx in range(len(time_lst)):
                # print(time_lst[date_idx], job_model[job_idx]['Start'], job_model[job_idx]['End'])
                if is_inside_interval(time_lst[date_idx], job_model[job_idx]['Start'], job_model[job_idx]['End']) == True:
                    model_for_job[date_idx][1] += 1
                    model_for_ncpu[date_idx][1] += int(job_model[job_idx]['NCPUS'])
    return [model_for_job, model_for_ncpu]