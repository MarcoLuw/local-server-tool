import QtQuick.Dialogs 1.3
import QtQuick 2.12
import QtQuick 2.15
import QtQuick 2.5
import QtQuick.Window 2.12
import QtQuick.Window 2.2
import QtQuick.Layouts 1.1
import QtQuick.Controls 1.1
import QtQuick.Controls 1.4
import QtQuick.Controls 1.5
import QtQuick.Controls 2.15
import QtQml.Models 2.15
import QtGraphicalEffects 1.15
import QtCharts 2.15
import "UI_Component"

Window  {
    id: root
    visible: true
    width: Screen.width/2
    height: Screen.width/3
    minimumWidth: 800
    minimumHeight: 600  
    title: qsTr("Akselos Server Management")

    onClosing: backend.close_all_connection()

    // ------ get state in order to viewing color ---- //
    function getStateColor(num) {
        if (num === 0) return "black";
        else if (num === 1) return "orangered";
        else return "green";
    }
    
    /*-------------------------------------Wrapper Content------------------------------------*/

    /* ----------------------------------------------------------------
    ----------------------------- Navigation Bar ----------------------
    ------------------------------------------------------------------- */
    Rectangle {
        id: nav_bar
        width: root.width; height: root.height/11
        //color: "aquamarine"
        color: "#116A7B"
        // border.color: "black"

        RowLayout {
            spacing: 0
            anchors.fill: parent

            Rectangle {
                id: logo
                width: 1/4 * nav_bar.width
                height: nav_bar.height
                radius: 10
                color: "#116A7B"

                Layout.alignment: Qt.AlignLeft | Qt.AlignVCenter
                Layout.margins: 10

                Image {
                    id: akselos_logo
                    source: "image/logo.png"
                    width: parent.width
                    height: parent.height
                    fillMode: Image.PreserveAspectCrop
                    visible: true
                    //anchors.centerIn: parent
                }
            }

            RoundButton {
                id: user
                implicitWidth: 1/6 * nav_bar.width
                implicitHeight: nav_bar.height/3

                Layout.alignment: Qt.AlignRight | Qt.AlignVCenter
                Layout.rightMargin: 15

                //highlighted: true
                text: "Check connection for all servers"
                font.pixelSize: user.width/20
                font.weight: Font.Bold
                onClicked: {
                    backend.pingMultiHosts()
                }
                background: Rectangle {
                    color: '#CDC2AE'
                    radius: parent.radius
                }
            }
        }
    }

    /* ----------------------------------------------------------------
    ----------------------------- Content Area ------------------------
    ------------------------------------------------------------------- */
    Rectangle {
        id: content_wrapper
        width: root.width
        height: 1/3 * root.height
        //color: "red"
        anchors.top: nav_bar.bottom

        Row {
            spacing: 0

            //-------------Server status area-----------------//
            Rectangle {
                id: ctl_leftSide
                width: content_wrapper.width/4
                height: content_wrapper.height
                //color: "bisque"
                color: "#C2DEDC"
                // border.color: "black"

                Column {
                    id: server_container

                    //----function handler for choosing server---//
                    function toggleSelection(rectangle, index) {
                        if (activeRectangle === rectangle) {
                            activeRectangle = null
                        }
                        else {
                            activeRectangle = rectangle
                            selectedServerIndex = index
                        }
                    }
                    property var activeRectangle: null
                    property var selectedServerIndex: -1
                    //----------------------------------------//

                    anchors.horizontalCenter: ctl_leftSide.horizontalCenter
                    anchors.top: ctl_leftSide.top
                    anchors.topMargin: 10
                    
                    // List of server
                    ListView {
                        id: server_name_list
                        width: ctl_leftSide.width
                        height: 5.5/7 * ctl_leftSide.height
                        spacing: 8
                        clip: true

                        model: backend.stateColor
                        delegate: Rectangle {
                            id: server_name
                            width: ctl_leftSide.width - 20
                            height: 1/7 * ctl_leftSide.height
                            anchors.horizontalCenter: parent.horizontalCenter
                            
                            border.color: "black"
                            radius: 5
                            color: isActive ? "#ECE5C7" : "White"

                            property bool isActive: server_container.activeRectangle === this
                            property string name_of_server: text_of_name.text
                            property alias status_dot_ssh: status_dot.color

                            MouseArea {
                                anchors.fill: parent
                                onClicked: {
                                    server_container.toggleSelection(server_name)
                                    if (isActive) {
                                        console.log("Server name: ", modelData.name)

                                        // Switch to active server
                                        server_name_list.currentIndex = index
                                        backend.switchClient(modelData.name)
                                    }
                                }
                            }

                            // Name of Server
                            Text {
                                id: text_of_name
                                text: modelData.name
                                font.pixelSize: 1/2.5 *  server_name.height
                                anchors.centerIn: parent
                            }
                            // Status Dot
                            Rectangle {
                                id: status_dot
                                width: height
                                height: 1/4 * server_name.height
                                radius: Math.min(status_dot.width, status_dot.height) / 2
                                anchors.left: server_name.left
                                anchors.verticalCenter: server_name.verticalCenter
                                anchors.leftMargin: 20
                                color: getStateColor(modelData.state)
                            }
                            // Delete Button
                            Button {
                                id: delServer
                                width: height
                                height: 9/20 * server_name.height
                                anchors.right: server_name.right
                                anchors.verticalCenter: server_name.verticalCenter
                                anchors.rightMargin: 10
                                background: Rectangle {
                                    color: "#ba1e1e"
                                }
                                Image {
                                    source: "image/delete_icon.png"
                                    anchors.fill: parent           // Make the image fill the button completely
                                    fillMode: Image.PreserveAspectFit
                                }
                                onClicked: delPopup.open()
                            }
                            // Yes/No Popup
                            Popup {
                                id: delPopup
                                dim: true
                                anchors.centerIn: Overlay.overlay
                                width: 400
                                ColumnLayout {
                                    anchors.fill: parent
                                    spacing: 30
                                    Text {
                                        text: "Are you sure to delete ?"
                                        font.pixelSize: 22
                                        font.weight: Font.Bold
                                    }
                                    RowLayout {
                                        spacing: 10
                                        Layout.alignment: Qt.AlignRight
                                        RoundButton {
                                            id: yes_btn
                                            Layout.fillHeight: true
                                            implicitWidth: delPopup.width * 1/4
                                            radius: width/2
                                            text: "Yes"
                                            font.pixelSize: 18

                                            background: Rectangle {
                                                color: "#ede8e8"
                                                radius: yes_btn.radius
                                                border.color: "black"
                                            }
                                            onClicked: {
                                                if (isActive)
                                                    backend.delete_server(modelData.name)
                                            }
                                        }
                                        RoundButton {
                                            id: no_btn
                                            text: "No"
                                            Layout.fillHeight: true
                                            implicitWidth: yes_btn.width
                                            font.pixelSize: 18

                                            contentItem: Text {
                                                text: no_btn.text
                                                font: no_btn.font
                                                color: "white"
                                                horizontalAlignment: Text.AlignHCenter
                                                verticalAlignment: Text.AlignVCenter
                                                elide: Text.ElideRight
                                            }
                                            background: Rectangle {
                                                color: "#ba1e1e"
                                                radius: yes_btn.radius
                                                border.color: "black"
                                            }
                                            onClicked: delPopup.close()
                                        }
                                    }
                                }
                                closePolicy: Popup.NoAutoClose
                            }
                        }
                    }
                    
                    //---------- Adding Button -----------//
                    RoundButton {
                        id: add_btn
                        width: ctl_leftSide.width - 20
                        height: 1/7 * ctl_leftSide.height
                        anchors.horizontalCenter: parent.horizontalCenter

                        radius: 5
                        text: "+"
                        font.pixelSize: 40

                        onClicked: {
                            addServerPopup.open()
                        }

                        background: Rectangle {
                            color: '#CDC2AE'
                        }
                    }
                    /* Popup for adding more server */
                    Popup {
                        id: addServerPopup
                        width: contentWidth + 50
                        height: contentHeight + 50
                        anchors.centerIn: Overlay.overlay
                        contentItem: Rectangle {
                            ColumnLayout {
                                spacing: 20
                                anchors.centerIn: parent
                                RowLayout {
                                    spacing: 20
                                    Text {
                                        text: 'Server Name'
                                        font.pixelSize: 18
                                    }
                                    TextField {
                                        id: serverNameInput
                                        font.pixelSize: 16
                                        placeholderText: "Your server name"
                                        selectByMouse: true
                                    }
                                }
                                RowLayout {
                                    spacing: 35
                                    Text {
                                        text: 'IP address'
                                        font.pixelSize: 18
                                    }
                                    TextField {
                                        id: ipInput
                                        font.pixelSize: 16
                                        placeholderText: "Server's IP address"
                                        selectByMouse: true
                                    }
                                }
                                RowLayout {
                                    spacing: 39
                                    Text {
                                        text: 'Username'
                                        font.pixelSize: 18
                                    }
                                    TextField {
                                        id: usernameInput
                                        // Layout.fillWidth: true
                                        // width: parent.width
                                        //horizontalAlignment: TextInput.AlignHCenter
                                        // anchors.verticalCenter: parent.verticalCenter
                                        font.pixelSize: 16
                                        placeholderText: "Username in Server"
                                        //overwriteMode: true
                                        selectByMouse: true
                                        // wrapMode: TextInput.Wrap
                                    }
                                }
                                Button {
                                    text: "Add"
                                    Layout.alignment: Qt.AlignRight

                                    background: Rectangle {
                                        color: '#CDC2AE'
                                    }
                                    onClicked: {
                                        var name = serverNameInput.displayText
                                        var ip = ipInput.displayText
                                        var user = usernameInput.displayText
                                        backend.update_server_list(name, ip, user)
                                        //server_name_list.model = backend.stateColor
                                        addServerPopup.close()
                                    }
                                }
                            }
                        }
                    }
                }
            }

            //---------------Action Area----------------
            Rectangle {
                id: ctl_rightSide
                width: 3/4 * content_wrapper.width
                height: content_wrapper.height
                //color: "cadetblue"
                color: "#C2DEDC"
                // border.color: "black"

                /* Get information part */
                RowLayout {
                    anchors.fill: parent
                    spacing: 10

                    // SSH connection
                    RoundButton {
                        id: ssh_btn
                        implicitWidth: 1/7 * ctl_rightSide.width
                        height: 1/3 * width

                        // Layout.fillWidth: true
                        Layout.alignment: Qt.AlignTop
                        Layout.topMargin: 10
                        Layout.leftMargin: 10

                        background: Rectangle {
                            color: '#CDC2AE'
                            radius: parent.radius
                        }
                        text: "SSH Connect"
                        font.pixelSize: 16
                        font.weight: Font.Bold

                        // Get passphrase Popup
                        Popup {
                            id: get_pass
                            width: 1/3 * ctl_rightSide.width
                            height: 1/3 * width
                            anchors.centerIn: Overlay.overlay

                            MessageDialog {
                                id: dialog_notice
                                title: "Warning"
                                text: "SSH liken't u, don't connect anymore -.- !"
                                icon: StandardIcon.Critical
                                standardButtons: StandardButton.Close
                            }

                            MessageDialog {
                                id: dialog_notice_1
                                title: "Warning"
                                text: "Connection Failed"
                                icon: StandardIcon.Critical
                                standardButtons: StandardButton.Close
                            }

                            contentItem: ColumnLayout {
                                RowLayout {
                                    Layout.alignment: Qt.AlignTop
                                    anchors.fill: parent
                                    Label {
                                        id: pass_label
                                        width: pass_label.implicitWidth
                                        text: "Input your passphrase"
                                        font.pixelSize: 16
                                    }

                                    Rectangle {
                                        id: pass_wrapper
                                        width: 4/5 * get_pass.width
                                        height: pass_label.height + 20
                                        
                                        anchors.left: pass_label.right
                                        anchors.leftMargin: 10
                                        anchors.right: parent.right
                                        border.color: "black"

                                        TextInput {
                                            id: passpharse_input
                                            width: pass_wrapper.width
                                            height: pass_wrapper.height

                                            echoMode: TextInput.Password
                                            selectByMouse: true
                                        }
                                    }
                                }
                                
                                //----Import pass button----
                                Button {
                                    id: import_pass_btn
                                    implicitWidth: 1/3 * parent.width
                                    height: pass_wrapper.height
                                    Layout.alignment: Qt.AlignRight | Qt.AlignBottom

                                    background: Rectangle {
                                        color: '#CDC2AE'
                                    }
                                    font.pixelSize: 17
                                    text: "Connect"
                                    font.weight: Font.Bold

                                    onClicked: {
                                        var serverName = server_name_list.itemAtIndex(server_name_list.currentIndex).name_of_server
                                        var passwd = passpharse_input.text
                                        // console.log(serverName, passwd)
                                        //console.log(backend.ssh_initial(serverName, passwd))
                                        if (!backend.check_ssh_connection(serverName)) {
                                            dialog_notice.open()
                                        }
                                        else {
                                            if (!backend.hasSSH(serverName, passwd))
                                                dialog_notice_1.open()
                                            else {}
                                        }
                                        backend.save_passphrase(passwd)
                                        backend.manually_check_connection()
                                        
                                        get_pass.close()
                                    }                         
                                }
                            }
                            closePolicy: Popup.CloseOnEscape
                        }

                        onClicked: {
                            get_pass.open()
                        }
                    }

                    // Get key
                    RoundButton {
                        id: import_key_btn
                        implicitWidth: 1/7 * ctl_rightSide.width
                        height: 1/3 * width

                        // Layout.fillWidth: true
                        anchors.left: ssh_btn.right
                        anchors.leftMargin: 10
                        Layout.alignment: Qt.AlignTop
                        Layout.topMargin: 10

                        background: Rectangle {
                            color: '#CDC2AE'
                            radius: parent.radius
                        }
                        font.pixelSize: 16
                        text: "Import private key"
                        font.weight: Font.Bold

                        // Get key path Popup
                        Popup {
                            id: get_key_path
                            width: 1/3 * ctl_rightSide.width
                            height: 1/3 * width

                            leftInset: -10
                            rightInset: -10
                            anchors.centerIn: Overlay.overlay

                            contentItem: ColumnLayout {
                                Label {
                                    id: key_path_label
                                    width: pass_label.implicitWidth
                                    text: "Your file path:"
                                    font.pixelSize: 16
                                }

                                Rectangle {
                                    id: key_path_wrapper
                                    height: key_path_label.height + 20
                                    
                                    anchors.left: parent.left
                                    anchors.right: parent.right
                                    border.color: "black"

                                    TextInput {
                                        id: key_path_input
                                        width: key_path_wrapper.width
                                        height: key_path_wrapper.height

                                        verticalAlignment: TextInput.AlignVCenter
                                        font.pixelSize: key_path_input.height - 20
                                        echoMode: TextInput.Normal
                                        selectByMouse: true
                                    }
                                }

                                //----Import key file button----
                                Button {
                                    id: import_path_btn
                                    implicitWidth: 1/3 * parent.width
                                    height: parent.height
                                    Layout.alignment: Qt.AlignRight | Qt.AlignBottom

                                    background: Rectangle {
                                        color: '#CDC2AE'
                                    }
                                    font.pixelSize: 17
                                    text: "Apply"
                                    font.weight: Font.Bold

                                    onClicked: {
                                        console.log(key_path_input.text)
                                        backend.updateFilePath(key_path_input.text)
                                        get_key_path.close()
                                    }                         
                                }
                            }
                            closePolicy: Popup.CloseOnEscape
                        }

                        onClicked: {
                            get_key_path.open()
                        }
                    }

                    // Get username
                    RoundButton {
                        id: choose_user_btn
                        implicitWidth: 1/7 * ctl_rightSide.width
                        height: 1/3 * width

                        // Layout.fillWidth: true
                        anchors.left: import_key_btn.right
                        anchors.leftMargin: 10
                        Layout.alignment: Qt.AlignTop
                        Layout.topMargin: 10

                        background: Rectangle {
                            color: '#CDC2AE'
                            radius: parent.radius
                        }
                        font.pixelSize: 16
                        text: "IMPORT username"
                        font.weight: Font.Bold
                        visible: server_name_list.itemAtIndex(server_name_list.currentIndex).isActive

                        Popup {
                            id: get_user
                            width: 1/3 * ctl_rightSide.width
                            height: 1/3 * width

                            leftInset: -10
                            rightInset: -10
                            anchors.centerIn: Overlay.overlay

                            contentItem: ColumnLayout {
                                //Layout.alignment: Qt.AlignTop
                                //anchors.fill: parent
                                //spacing: 50
                                Label {
                                    id: get_user_label
                                    width: get_user_label.implicitWidth
                                    text: "Add your user:"
                                    font.pixelSize: 16
                                }

                                Rectangle {
                                    id: get_user_wrapper
                                    //width: get_pass.width - pass_label.width
                                    //width: 200
                                    height: get_user_label.height + 20
                                    
                                    anchors.left: parent.left
                                    anchors.right: parent.right
                                    border.color: "black"

                                    TextInput {
                                        id: get_user_input
                                        width: get_user_wrapper.width
                                        height: get_user_wrapper.height

                                        verticalAlignment: TextInput.AlignVCenter
                                        font.pixelSize: key_path_input.height - 20
                                        echoMode: TextInput.Normal
                                        selectByMouse: true
                                    }
                                }

                                //----Import key file button----
                                Button {
                                    id: import_user_btn
                                    implicitWidth: 1/3 * parent.width
                                    height: parent.height
                                    Layout.alignment: Qt.AlignRight | Qt.AlignBottom

                                    background: Rectangle {
                                        color: '#CDC2AE'
                                    }
                                    font.pixelSize: 17
                                    text: "Apply"
                                    font.weight: Font.Bold

                                    onClicked: {
                                        console.log(get_user_input.text)
                                        var svrActive_name = server_name_list.itemAtIndex(server_name_list.currentIndex).name_of_server
                                        console.log("Server name: ", svrActive_name)
                                        backend.updateUsername(svrActive_name, get_user_input.text)

                                        get_user.close()
                                    }                         
                                }
                            }
                            closePolicy: Popup.CloseOnEscape
                        }
                        onClicked: {
                            get_user.open()
                        }
                    }

                    // Action label
                    Rectangle {
                        id: action
                        width: 1/6 * ctl_rightSide.width
                        height: 1/3 * width
                        
                        color: "#C2DEDC"
                        border.color: "black"

                        //Layout.alignment: Qt.AlignLeft | Qt.AlignTop
                        anchors.right: option_wrapper.left
                        anchors.top: parent.top
                        anchors.margins: 10
                        
                        Text {
                            text: "ACTION"
                            font.pixelSize: 1/3 * parent.height + 4
                            anchors.centerIn: parent
                            font.weight: Font.Bold
                        }
                    }
                    
                    // List of action
                    Rectangle {
                        id: option_wrapper
                        width: 1/3 * ctl_rightSide.width
                        height: action.height
                        border.width: 2
                        border.color: "black"
                        
                        Layout.alignment: Qt.AlignRight | Qt.AlignTop
                        anchors.right: parent.right
                        anchors.top: parent.top
                        anchors.margins: 10
                        //Layout.fillWidth: true

                        ComboBox {
                            id: action_box
                            width: option_wrapper.width - 10
                            height: option_wrapper.height - 10
                            anchors.centerIn: parent
                            background: Rectangle {
                                color: 'white'
                            }
                            model: ['overall-checking', 
                                    'check-disk-filter',
                                    'check-who-connecting',
                                    'check-hosting-serivce',
                                    'check-https-port',
                                    'check-jobs-state', 
                            ]

                            onActivated: {
                                console.log("Selected option: ", action_box.currentText)
                                console.log("Selected index: ", action_box.currentIndex)
                            }
                        }
                    }

                    // Action button
                    Button {
                        id: apply_btn
                        implicitWidth: 1/8 * ctl_rightSide.width
                        implicitHeight: action.height - 5

                        // Layout.alignment: Qt.AlignRight
                        anchors.top: option_wrapper.bottom
                        anchors.right: parent.right
                        anchors.margins: 10

                        text: "Apply"
                        font.pixelSize: 17
                        font.weight: Font.Bold
                        background: Rectangle {
                            color: '#CDC2AE'
                        }
                        onClicked: {
                            /* index 0 */
                            if (action_box.currentText === 'overall-checking') {
                                disk_table.model = backend.disk_data_filter_model()
                                user_table.model = backend.who_data_model()
                                var serverName = server_name_list.itemAtIndex(server_name_list.currentIndex).name_of_server
                                var result = backend.check_https_port_state(serverName)
                                if (result[0].includes("Open"))
                                    https_port_text.text = "Service on port " + result[1] + " is working!"
                                else 
                                    https_port_text.text = "Service on port " + result[1] + " is closing!"
                                // index 3
                                service_table.model = backend.host_service_data_model()
                            }
                            /* index 1 */
                            else if (action_box.currentText === 'check-disk-filter') {
                                disk_table.model = backend.disk_data_model()
                            }
                            /* index 2 */
                            else if (action_box.currentText === 'check-who-connecting') {
                                user_table.model = backend.who_data_model()
                            }
                            /* index 3 */
                            else if (action_box.currentText === 'check-https-port') {
                                var serverName = server_name_list.itemAtIndex(server_name_list.currentIndex).name_of_server
                                var result = backend.check_https_port_state(serverName)

                                if (result[0].includes("Open"))
                                    https_port_text.text = "Service on port " + result[1] + " is working!"
                                else 
                                    https_port_text.text = "Service on port " + result[1] + " is closing!"
                            }
                            /* index 4 */
                            else if (action_box.currentText === 'check-hosting-serivce') {
                                service_table.model = backend.host_service_data_model()
                            }
                            /* index 5 */
                            else if (action_box.currentText === 'check-jobs-state') {
                                //job_table.model = backend.get_job_state(2)
                            }
                        }
                    }
                }
            }
        }
    }

    /* ----------------------------------------------------------------
    ----------------------------- Output Area -------------------------
    ------------------------------------------------------------------- */
    Rectangle {

        id: output_area
        width: root.width
        height: root.height - nav_bar.height - content_wrapper.height
        anchors.top: content_wrapper.bottom
        //color: "blueviolet"
        color: "#C2DEDC"
        border.color: "black"

        /* ------ Overall checking area ------- */
        GridLayout {
            id: grid_content
            anchors.fill: parent
            columns: 2

            /* define tableviewcolumn component */
            Component {
                id: cmp
                TableViewColumn {
                    id: table_col
                    horizontalAlignment: Text.AlignHCenter
                }
            }

            /* ---- Table for DISK ----- */
            TableView {
                id: disk_table
                Layout.fillWidth: true
                Layout.fillHeight: true
                visible: action_box.currentIndex === 1 | action_box.currentIndex === 0

                // Row spacing
                rowDelegate: Rectangle {
                    height: 25
                }

                itemDelegate: Label {
                    text: styleData.value
                    font.pixelSize: 18
                    color: {
                        if (styleData.role === "Use%") 
                            if (parseFloat(styleData.value.slice(0, 2)) > 90) 
                                return "red"
                        return "black"
                    }
                    background: MouseArea {
                        id: mouseArea_header
                        anchors.fill: parent
                        enabled: action_box.currentIndex === 1
                        acceptedButtons: Qt.LeftButton | Qt.RightButton
                        onClicked: { 
                            if (mouse.button === Qt.RightButton){
                                dirStorage.popup()
                            }
                        }
                        Menu {
                            id: dirStorage
                            MenuItem { 
                                id: show_dir_storage
                                text: "Show internal directory largest size"
                                onTriggered: {
                                    barProgress_popup.open()
                                    disk_table.startIncrease()
                                    backend.handle_disk_data(modelData.Mounted)
                                }
                            }
                        }
                    }
                }

                /* ------- START bar progress part ----- */
                /* Pop up window for PROGRESS BAR */
                Popup {
                    id: barProgress_popup
                    modal: true
                    anchors.centerIn: Overlay.overlay
                    padding: 0
                    contentItem: ProgressBar {
                        id: control
                        value: 0.0
                        padding: 4

                        background: Rectangle {
                            implicitWidth: 1/2 * disk_table.width
                            implicitHeight: 1/15 * disk_table.height
                            color: "#e6e6e6"
                            radius: 10
                        }

                        contentItem: Item {
                            implicitWidth: 1/2 * disk_table.width
                            implicitHeight: 1/15 * disk_table.height - 15

                            Rectangle {
                                width: control.visualPosition * parent.width
                                height: parent.height
                                radius: 9
                                color: "#17a81a"
                                Text {
                                    text: {
                                        var res = Math.round(control.value * 100)
                                        return res + '%'
                                    }
                                    font.pixelSize: 18
                                    font.weight: Font.Bold
                                    anchors.centerIn: parent
                                }
                            }
                        }
                        anchors.centerIn: parent
                    }
                    closePolicy: Popup.CloseOnEscape
                }
                property bool isIncrease: false

                /* Timer for updating Progress Bar */
                Timer {
                    id: timer
                    interval: backend.interval_value
                    running: disk_table.isIncrease
                    repeat: true
                    onTriggered: {
                        disk_table.isIncrease = true
                        if (timer.interval === 3000) {
                            if (control.value <  0.5) 
                                control.value += 0.01 // Increment value gradually
                        }
                        else if (timer.interval === 1000) {
                            if (control.value < 0.8) 
                                control.value += 0.01
                        }
                        else if (timer.interval === 10) {
                            if (control.value < 1) 
                                control.value += 0.01
                            else {
                                barProgress_popup.close()
                                largest_dir_table.model = backend.largest_model_value
                                showDirPopup.open()
                                backend.reset_model_interval()
                                disk_table.isIncrease = false
                            }
                        }
                    }
                }

                // Start the progress bar
                function startIncrease() {
                    console.log("I am called")
                    if (!disk_table.isIncrease) {
                        control.value = 0.0 // Reset value before increasing
                        // disk_table.isIncrease = true
                        timer.start()
                    }
                }
                /* ------- END bar progress part ----- */

                /* ------- START Table display disk storage PART --- */
                // ------- Pop up for Table display disk storage --- //
                Popup {
                    id: showDirPopup
                    width: 700
                    height: 700
                    modal: true
                    anchors.centerIn: Overlay.overlay
                    enter: Transition {
                        NumberAnimation { property: "opacity"; from: 0.0; to: 1.0 }
                    }
                    contentItem: TableView {
                        id: largest_dir_table
                        rowDelegate: Rectangle {
                            height: 30
                        }
                        itemDelegate: Label {
                            text: styleData.value
                            font.pixelSize: 18
                        }

                        /* Update Table Cell */
                        Component.onCompleted: {
                            var headers = [
                                {role: 'name', title: 'Directory Name'},
                                {role: 'size', title: 'Size'}
                            ]
                            for (var i = 0; i < headers.length; i++)
                                largest_dir_table.addColumn(cmp.createObject(largest_dir_table, headers[i]))
                        }
                    }
                    closePolicy: Popup.CloseOnEscape | Popup.CloseOnPressOutside
                }
                // ------- END Table display disk storage PART --- //

                /* Update Table Cell */
                Component.onCompleted: {
                    var headers = [
                        {role: "Filesystem", title: "File System"},
                        {role: "Size", title: "Size"},
                        {role: "Used", title: "Used"},
                        {role: "Avail", title: "Avail"},
                        {role: "Use%", title: "Use%"},
                        {role: "Mounted", title: "Mounted"}
                    ]
                    for (var i=0; i< headers.length; i++) {
                        disk_table.addColumn(cmp.createObject(disk_table, headers[i]))
                    }
                }
            }

            /* ---- Table for WHO - USER ----- */
            TableView {
                id: user_table
                Layout.fillWidth: true
                Layout.fillHeight: true
                visible: action_box.currentIndex === 2 | action_box.currentIndex === 0
                rowDelegate: Rectangle {
                    height: 30
                }

                itemDelegate: Label {
                    text: styleData.value
                    font.pixelSize: 18
                    horizontalAlignment: Text.AlignHCenter
                    color: {
                        if (styleData.role === "IP address") {
                            if (!styleData.value.includes("192.168"))
                                return "red"
                        }
                    }
                }
                /* Update Table Cell */
                Component.onCompleted: {
                    var headers = [
                        {role: 'Username', title: 'Username'},
                        {role: 'Terminal No', title: 'Terminal No'},
                        {role: 'Login Time', title: 'Login Time'},
                        {role: 'Activity', title: 'Activity'},
                        {role: 'IP address', title: 'IP address'}
                    ]

                    for (var i = 0; i < headers.length; i++)
                        user_table.addColumn(cmp.createObject(user_table, headers[i]))
                }
            }
            
            /* ---- Table for SERVICE CHECKING ----- */
            TableView {
                id: service_table
                Layout.fillWidth: true
                Layout.fillHeight: true
                visible: action_box.currentIndex === 3 | action_box.currentIndex === 0
                rowDelegate: Rectangle {
                    height: 30
                }

                // Pop up for more information
                Popup {
                    id: service_popup
                    width: parent.width * 2/3
                    height: parent.height
                    anchors.centerIn: Overlay.overlay
                    contentItem: ScrollView {
                        anchors.fill: parent
                        Text {
                            id: service_state
                            font.pixelSize: 18
                            anchors.centerIn: parent
                            font.family: "New Courier"
                        }
                        clip: true
                    }
                }

                itemDelegate: Label {
                    text: styleData.value
                    horizontalAlignment: Text.AlignHCenter
                    color: {
                        if (styleData.role === "State") {
                            if (styleData.value.includes("active")) {
                                return "green"
                            }
                            else return "red"
                        }
                        return "black"
                    }
                    font.weight: {
                        if (styleData.role === "State") {
                            return Font.DemiBold
                        }
                    }
                    font.pixelSize: 18
                    background: MouseArea {
                        id: mouseArea_service
                        anchors.fill: parent
                        enabled: action_box.currentIndex === 3
                        acceptedButtons: Qt.LeftButton | Qt.RightButton
                        property var state_res: null
                        onClicked: {
                            hostService.popup()
                            state_res = backend.get_detail_hosting_service()
                        }
                        Menu {
                            id: hostService
                            MenuItem { 
                                id: show_host_service
                                text: "Show detail status"
                                onTriggered: {
                                    service_state.text = mouseArea_service.state_res[styleData.row]
                                    service_popup.open()
                                }
                            }
                        }
                    }
                }
                /* Update Table Cell */
                Component.onCompleted: {
                    var headers = [
                        {role: 'Service', title: 'Hosting Service'},
                        {role: 'State', title: 'State'}
                    ]
                    for (var i = 0; i < headers.length; i++)
                        service_table.addColumn(cmp.createObject(service_table, headers[i]))
                }
            }

            /* ---- Rectangle showing HTTPS-PORT STATE ----- */
            Rectangle {
                id: https_port
                Layout.fillWidth: true
                Layout.fillHeight: true
                color: "white"
                border.color: "black"
                visible: action_box.currentIndex === 4 | action_box.currentIndex === 0
                Text {
                    id: https_port_text
                    font.pixelSize: 18
                    anchors.centerIn: parent
                    wrapMode: Text.Wrap
                    clip: true
                }
            }

            /* ---- Layout showing Job Checking ----- */
            ColumnLayout {
                visible: action_box.currentIndex === 5
                Layout.fillWidth: true
                Layout.fillHeight: true
                Layout.topMargin: 20
                spacing: 10

                RowLayout {
                    id: layout_datetime
                    spacing: 30
                    Layout.alignment: Qt.AlignRight
                    // anchors.right: parent.right
                    Layout.rightMargin: 15
                    
                    /* Function to change date's format */
                    function filter_date(date, time) {
                        var dateString = date.toISOString()
                        const myArray = dateString.split("T")
                        var date_time
                        if (time < 10)
                            date_time = myArray[0] + 'T' + '0' + time + ':00'
                        else
                            date_time = myArray[0] + 'T' + time + ':00'
                        return date_time
                    }

                    /* Custom Button for ChartView  */
                    Rectangle {
                        width: output_area.width * 1/6
                        height: time_btn.height
                        color: '#1c8723'
                        radius: 8
                        border.color: "#4d491c"
                        border.width: 3
                        Text {
                            text: "View Chart"
                            color: "White"
                            font.pixelSize: 16
                            font.weight: Font.Bold
                            anchors.centerIn: parent
                        }
                        MouseArea {
                            anchors.fill: parent
                            onClicked: {
                                chartPopup.open()
                            }
                        }
                    }

                    /* Popup for showing ChartView */
                    Popup {
                        id: chartPopup
                        width: output_area.width * 4/5
                        height: output_area.height
                        anchors.centerIn: Overlay.overlay
                        dim: true

                        function getNumberDayInMonth() {
                            // Create a new Date object representing the current date
                            var currentDate = new Date();
                            // Get the month (0-indexed) and year of the current date
                            var currentMonth = currentDate.getMonth();
                            var currentYear = currentDate.getFullYear();
                            // Create a new Date object for the next month's 0th day
                            var nextMonthFirstDay = new Date(currentYear, currentMonth + 1, 0);
                            // Get the number of days in the current month
                            var numberOfDaysInCurrentMonth = nextMonthFirstDay.getDate();
                            // console.log("Number of days in the current month:", numberOfDaysInCurrentMonth);
                            return numberOfDaysInCurrentMonth
                        }

                        contentItem: ColumnLayout {
                            spacing: 20

                            // Setting custom interval
                            RowLayout {
                                spacing: 20
                                Layout.fillWidth: true
                                Label {
                                    text: "Set Interval"
                                    font.pixelSize: 20
                                    font.weight: Font.Bold
                                }

                                SpinBox {
                                    id: intervalInput
                                    from: 2
                                    to: 100
                                    editable: true
                                }

                                RoundButton {
                                    text: "Set"
                                    font.pixelSize: 16
                                    highlighted: true
                                    onClicked: {
                                        var interval = Number(intervalInput.displayText)
                                        console.log("main ", interval)
                                        backend.get_line_chart_data_hour(date_input.displayText)
                                        tabHourChart.item.plot_soil_illus(interval)
                                        backend.get_line_chart_data_day(date_input.displayText)
                                        tabDayChart.item.plot_soil_illus(interval)
                                        backend.get_interval_line_chart(date_input.displayText, end_date_input.displayText, interval)
                                        tabIntervalChart.item.plot_soil_illus_interval(interval)
                                    }
                                }
                            }

                            // Tab for ChartView of Job Monitoring
                            TabView {
                                id: chartTab
                                Layout.fillWidth: true
                                Layout.fillHeight: true
                                Tab {
                                    id: tabHourChart
                                    // active: true
                                    title: 'Day Chart'
                                    LineChartView {
                                        id: lineChartView_hour
                                        anchors.fill: parent

                                        /* Initial ChartData and Get ChartData */
                                        Component.onCompleted: {
                                            backend.get_line_chart_data_hour(date_input.displayText)
                                            lineChartView_hour.plot_soil_illus(24)
                                        }
                                    }
                                }
                                Tab {
                                    id: tabDayChart
                                    // active: true
                                    title: 'Month Chart'
                                    LineChartView {
                                        id: lineChartView_day
                                        anchors.fill: parent

                                        /* Initial ChartData and Get ChartData */
                                        Component.onCompleted: {
                                            backend.get_line_chart_data_day(date_input.displayText)
                                            lineChartView_day.plot_soil_illus(chartPopup.getNumberDayInMonth()+1)
                                        }
                                    }
                                }
                                Tab {
                                    id: tabUserChart
                                    title: 'User Chart'
                                    RowLayout {
                                        anchors.fill: parent
                                        spacing: 10
                                        PieChartView {
                                            id: pieChartView_job
                                            Layout.fillHeight: true
                                            width: parent.width / 2

                                            /* Get ChartData */
                                            Component.onCompleted: {
                                                pieChartView_job.illustratrate_pieChart(0)
                                            }
                                        }
                                        PieChartView {
                                            id: pieChartView_ncpu
                                            Layout.fillHeight: true
                                            width: parent.width / 2

                                            /* Get ChartData */
                                            Component.onCompleted: {
                                                pieChartView_ncpu.illustratrate_pieChart(1)
                                            }
                                        }
                                        /* Initial ChartData */
                                        Component.onCompleted: {
                                            backend.get_pie_chart_by_user(date_input.displayText)
                                        }
                                    }
                                }
                                Tab {
                                    id: tabIntervalChart
                                    // active: true
                                    title: 'Interval Job Chart'
                                    LineChartViewDateAxis {
                                        id: lineChartView_interval
                                        anchors.fill: parent

                                        /* Initial ChartData and Get ChartData */
                                        Component.onCompleted: {
                                            var interval = Number(intervalInput.displayText)
                                            backend.get_interval_line_chart(date_input.displayText, end_date_input.displayText, interval)
                                            lineChartView_interval.plot_soil_illus_interval(4)
                                        }
                                    }
                                }
                            }
                        }

                        /* Reset status of Tab --> false */
                        onClosed: {
                            tabHourChart.active = false
                            tabDayChart.active = false
                            tabUserChart.active = false
                            tabIntervalChart.active = false
                        }

                        /* Get ChartData and View them */
                        onOpened: {
                            lineChartView_hour.plot_soil_illus(24)
                            lineChartView_day.plot_soil_illus(chartPopup.getNumberDayInMonth()+1)
                            pieChartView_job.illustratrate_pieChart(0)
                            pieChartView_ncpu.illustratrate_pieChart(1)
                            lineChartView_interval.plot_soil_illus_interval(4)
                        }
                    }

                    /* StartTime input place */
                    Rectangle {
                        id: wrapper_date_input
                        width: output_area.width * 1/4
                        height: time_btn.height
                        visible: true
                        color: "white"
                        border.width: 1
                        radius: 5
                        // layer.enabled: true
                        // layer.effect: DropShadow {
                        //     transparentBorder: true
                        //     anchors.fill: parent
                        //     horizontalOffset: 3
                        //     verticalOffset: 3
                        //     radius: 5
                        //     samples: 20
                        //     color: "gray"
                        // }
                        TextInput  {
                            id: date_input
                            width: parent.width
                            horizontalAlignment: TextInput.AlignHCenter
                            anchors.verticalCenter: parent.verticalCenter
                            font.pixelSize: 16
                            //overwriteMode: true
                            selectByMouse: true
                            wrapMode: TextInput.Wrap
                            text: "Choose Start Date"
                            
                            onActiveFocusChanged: {
                                // console.log(date_input.text)
                                if (activeFocus)
                                    calendarPopup.open()
                            }
                        }
                        Popup {
                            id: calendarPopup
                            dim: false
                            y: wrapper_date_input.height + 20 + wrapper_date_input.y
                            contentItem: Calendar {
                                id: calen
                                minimumDate: new Date(2020, 0, 1)
                                maximumDate: new Date(2030, 0, 1)
                                onClicked: {
                                    console.log("from popup: ", selectedDate)
                                    date_input.text = layout_datetime.filter_date(calen.selectedDate, 0)
                                    console.log("from popup: ", date_input.text)
                                }
                            }
                            closePolicy: Popup.CloseOnEscape | Popup.CloseOnPressOutside | Popup.CloseOnPressOutsideParent	
                        }
                    }
                    
                    /* EndTime input place */
                    Rectangle {
                        id: wrapper_date_input_2
                        width: output_area.width * 1/4
                        height: time_btn.height
                        visible: true
                        color: "white"
                        border.width: 1
                        radius: 5
                        //layer.enabled: true
                        // layer.effect: DropShadow {
                        //     transparentBorder: true
                        //     anchors.fill: parent
                        //     horizontalOffset: 3
                        //     verticalOffset: 3
                        //     radius: 5
                        //     samples: 20
                        //     color: "gray"
                        // }
                        TextInput  {
                            id: end_date_input
                            width: parent.width
                            horizontalAlignment: TextInput.AlignHCenter
                            anchors.verticalCenter: parent.verticalCenter
                            font.pixelSize: 16
                            //overwriteMode: true
                            selectByMouse: true
                            wrapMode: TextInput.Wrap
                            text: "Choose End Date"
                            
                            onActiveFocusChanged: {
                                // console.log(date_input.text)
                                if (activeFocus)
                                    end_calendarPopup.open()
                            }
                        }
                        Popup {
                            id: end_calendarPopup
                            dim: false
                            y: wrapper_date_input_2.height + 20 + wrapper_date_input_2.y
                            contentItem: Calendar {
                                id: calen_2
                                minimumDate: new Date(2020, 0, 1)
                                maximumDate: new Date(2030, 0, 1)
                                onClicked: {
                                    console.log("from popup 2: ", selectedDate)
                                    end_date_input.text = layout_datetime.filter_date(calen_2.selectedDate, time_box.currentText)
                                    console.log("from popup 2: ", end_date_input.text)
                                }
                            }
                            closePolicy: Popup.CloseOnEscape | Popup.CloseOnPressOutside | Popup.CloseOnPressOutsideParent	
                        }
                    }
                    
                    /* HOUR Label */
                    Rectangle {
                        width: time_btn.width
                        height: time_btn.height
                        color: "#e9edf7"
                        Text {
                            text: "HOUR"
                            font.pixelSize: 16
                            font.weight: Font.DemiBold
                            anchors.centerIn: parent
                        }
                    }

                    /* List of Hour */
                    ComboBox {
                        id: time_box
                        width: option_wrapper.width - 10
                        height: option_wrapper.height - 10
                        model: 24
                        clip: true
                        onActivated: {
                            end_date_input.text = layout_datetime.filter_date(calen_2.selectedDate, time_box.currentText)
                        }
                    }

                    /* Apply Date Button */
                    RoundButton {
                        id: time_btn
                        text: "Update Table"
                        font.pixelSize: 18
                        font.weight: Font.Bold
                        background: Rectangle {
                            color: '#CDC2AE'
                            radius: parent.radius
                        }
                        onClicked: {
                            // console.log("hello it me ", date_input.displayText)
                            // console.log("hahahaha", tab1.item)
                            if (job_tabview.getTab(job_tabview.currentIndex).title.includes('FINISHED'))
                                tab1.item.model = backend.finish_job_model(date_input.displayText, end_date_input.displayText)
                            else
                                tab2.item.model = backend.running_job_model(date_input.displayText, end_date_input.displayText)
                        }
                    }
                }

                /* TabView for each TableView */
                TabView {
                    id: job_tabview
                    Layout.fillWidth: true
                    Layout.fillHeight: true

                    Tab {
                        id: tab1
                        title: 'FINISHED'
                        JobView {}
                    }
                    
                    Tab {
                        id: tab2
                        title: 'RUNNING'
                        JobView {}
                    }
                }
            }
        }
    }
}