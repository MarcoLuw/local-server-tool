import paramiko as pk
import getpass
import os
import json
import threading
import time

hostname = "192.168.1.154"
port = 22
username = "akselos_nda01"
private_key_file = "C://Users//hoang-anh//.ssh//id_ed25519"

ACTION_TO_COMMAND = {
    'check-disk': 'df -h',
    'check-service-quickview': 'service --status-all',
    'check-disk-filter': "df -h | awk 'NR==1 || (NR>1) {if ($5+0 > 90) {printf \"%s\\n\", $0} else {print}}'",
    'check-who-connecting': 'who -u',
    #'check-disk-filter': "df -h | awk 'NR==1 || (NR>1 && ($6 == \"/\" || $6 ~ /^\/mnt/)) {if ($5+0 > 90) {printf \"%s\\n\", $0} else {print}}'",
    'check-all-service': 'systemctl list-units --type=service',
    'check-ssh-service': 'systemctl status ssh',
    'check-who-by-log': 'tail /var/log/auth.log',
    'check-who-by-network': 'netstat -tn | grep ESTABLISHED',
    'check-running-jobs-inshell': 'jobs',
    'check-running-jobs-allserver': 'ps -ef',
    'check-running-jobs-curuser': 'ps -ef | grep $$',
    'custom-input': ''
}

# print output
def printout(client, input):
    stdin, stdout, stderr = client.exec_command(input)
    if stderr:
        print(stderr.read().decode())
    print(stdout.read().decode())
    #print("Type of output:", type(stdout.read().decode()))
    print("---------------------------------------")

def client_initial():
    try:
        client = pk.SSHClient()
        client.load_system_host_keys()
        client.set_missing_host_key_policy(pk.AutoAddPolicy())
        #client.connect(hostname=hostname, port=port, username=username, password=passwd)
        
        # using two-step authentification
        # getpass.getpass() to read password
        try:
            passphrase = getpass.getpass()
        except Exception as error:
            print('ERROR', error)
        
        client.connect(hostname=hostname, port=22, username=username, key_filename=private_key_file, passphrase=passphrase)

        print("Connect Successfully....")
        print("--------- Input your option --------")

        # -- Download file to remote host -- #
        # ssh = pk.SSHClient()
        # local_file_path = 'A://HoangAnh//local-server-tool//find_largest_dir.sh'
        # remote_file_path = 'find_largest_dir.sh'

        # with open(local_file_path, 'rb') as local_file:
        #     sftp = client.open_sftp()
        #     sftp.putfo(local_file, remote_file_path)
        #     sftp.close()

        return client
    except Exception:
        return False

def run_command(client):
    if client == False: return False
    while True:
        try:
            cmd = input()
            if cmd == "custom-input":
                while True:
                    custom_cmd = input("$> ")
                    if custom_cmd == "exit": break
                    printout(client, custom_cmd)
            if cmd == "exit": break
            if cmd in ACTION_TO_COMMAND:
                printout(client, ACTION_TO_COMMAND[cmd])
            else: print(f'There no command: {cmd}, please try again!')
            #stdin, stdout, stderr = client.exec_command(cmd)
        except KeyboardInterrupt:
            break

def run_one_command(client):
    stdin, stdout, stderr = client.exec_command('bash find_largest_dir.sh /mnt/DATA2')
    json_data = stdout.read().decode()
    # print(type(json_data))
    # print(json_data)
    data = json.loads(json_data)
    # print(data)
    hostname = "VN154"
    port_check = None

    for i in range(len(data["server_configs"])):
        if data["server_configs"][i]["hostname"] == hostname:
            port_check = data["server_configs"][i]["https_port"]
            break
    
    print(port_check)

    # file_path = os.path.join(os.path.dirname(__file__), 'disk.txt')
    # with open(file_path, "w") as file:
    #     file.write(disk_data)
    # rows = txt_data.split('\n')
    # who_data = [list(row.split()) for row in rows]
    # who_data_model = []

    # who_data = who_data[:-1]
    # for i in range(len(who_data)):
    #     who_data[i][6] = who_data[i][6][1:-1]
    #     who_data[i][2] = who_data[i][2] + ' ' + who_data[i][3]
    #     who_data[i].pop(3)
    #     who_data[i].pop(4)
    
    # header = ['Username', 'Terminal No', 'Loggin Time', 'Activity', 'IP address']
    # for i in range(len(who_data)):
    #     dict_tmp = {}
    #     for j in range(len(header)):
    #         dict_tmp[header[j]] = who_data[i][j]
    #     who_data_model.append(dict_tmp)

    # print(type(txt_data))

    # for i in range(len(disk_data)):
    #     if len(disk_data[i]) == 0: disk_data.pop(i)
        
    # for i in range(1, len(disk_data)):
    #     dict_tmp = {}
    #     for j in range(len(disk_data[0])):
    #         dict_tmp[disk_data[0][j]] = disk_data[i][j]
    #     disk_data_model.append(dict_tmp)
    # for ele in disk_data_model:
    #     print(ele)

def close(client):
    client.close()

# client = client_initial()
# close(client)
# run_command(client_initial())
# run_one_command(client_initial())


# data_to_list()
def run_one_command_2(client):
    stdin, stdout, stderr = client.exec_command('bash find_largest_dir.sh /mnt/DATA2')
    result = stdout.read().decode()
    return result

from anytree import Node, RenderTree, NodeMixin

def tach_chuoi(raw_data):
    # print(txt_file)
    rows = raw_data.split("\n")
    data1 = [list(row.split()) for row in rows][:-1]
    if data1[0][0] == '/': 
        data1[0][0] = '/root'

    for i in range(1,len(data1)):
        if data1[0][0] == '/root':
            data1[i][0] = '/root' + data1[i][0]
        data1[i][0] = data1[i][0].split("/")[1:]
    
    data1[0][0] = data1[0][0].split('/')[1:]
    if len(data1[0][0]) != 1:
        data1[0][0] = '/' + '/'.join(data1[0][0])
    
    # print(data1)
    # print("------------------------------------------")
    return data1

class myClass(NodeMixin):
    def __init__(self, name, length, parent = None, children = None):
        super(myClass, self).__init__()
        self.name = name
        self.length = length
        self.parent = parent
        if children:    self.children = children

def change_format(byte_number):
    units = ['B', 'MB', 'GB', 'TB', 'PB']
    size = int(byte_number)
    for i in range(len(units)):
        if size < 1024 or i == len(units) - 1:
            break
        size /= 1024
    size = round(size, 2)
    return f'{size} {units[i]}'


def construct_tree(raw_data):
    data = tach_chuoi(raw_data)
    nodes = []
    root_node = myClass(name=data[0][0][0] if len(data[0][0]) == 1 else data[0][0], length=data[0][1])
    nodes.append(root_node)
    # print(root_node.length)
    for i in range(1, len(data)):
        node_gen = myClass(name=data[i][0][len(data[i][0])-1], length=data[i][1], parent=nodes[len(nodes)-1])
        nodes.append(node_gen)
    
    result = ""

    for pre, _, node in RenderTree(root_node):
        # treestr = u"%s%s" % (pre, node.name)
        # print(treestr, node.length)
        result += f'{pre}{node.name} {change_format(node.length)} \n'
    return result

def run_one_command_3(client):
    stdin, stdout, stderr = client.exec_command('find /path/to/directory -type d -printf "%d %p\n" | sort -n | cut -d" "-f2-')
    result = stdout.read().decode()
    return result


# data = run_one_command_2(client_initial())
# print(data)
# print(construct_tree(data))

# while True:
#     event = threading.Event()
#     def process_raw_data(result):
#         time.sleep(3)
#         result += 2
#         print("result of sub2: ", result)


#     def get_data_remote():
#         time.sleep(10)
#         result = 10
#         print("result of sub1: ", result)
#         event.set()
#         sub2 = threading.Thread(target=process_raw_data, args=(result,))
#         sub2.start()
#         event.wait()

#     # Start the subthread to fetch the data
#     sub1 = threading.Thread(target=get_data_remote, daemon=True)
#     sub1.start()

#     # Continue main thread execution without waiting for subthread
#     # Perform other tasks here
#     print("Continue main thread execution...")
#     time.sleep(15)
#     print("Work cua main: 2000")

# client = client_initial()

def remove_files(json_data):
    if isinstance(json_data, list):
        return [remove_files(item) for item in json_data if item.get('type') not in ['file', 'link']]
    elif isinstance(json_data, dict):
        return {
            key: remove_files(value)
            for key, value in json_data.items()
            if not (key == 'contents' and json_data.get('type') in ['file', 'link'])
        }
    else:
        return json_data


def handle_data(disk_path):
    event = threading.Event()
    def process_raw_data(raw_data):
        event.wait()
        # Change interval faster
        print("Change to 1000ms")

        #self._set_interval(1000)

        # Handle data
        json_rm = raw_data.replace('{"error": "error opening dir"}', '')
        json_data = json.loads(json_rm)
        filter_data = remove_files(json_data)

        # Get list model for largest directories
        root = filter_data[0]
        dir_model = [{'name': root['name'], 'size': change_format(root['size'])}]
        i = 0
        tmp_dict = root
        while True:
            if 'contents' not in tmp_dict or len(tmp_dict['contents']) == 0:
                break
            tmp_dict = tmp_dict['contents'][0]
            dir_model.append({'name': tmp_dict['name'], 'size': change_format(tmp_dict['size'])})
            i += 1

        #self._set_largest_dir_model(dir_model)
        print("Change to 10ms")
        print(dir_model)
        #self._set_interval(10)

    def get_data_remote():
        command = f"tree {disk_path} -a -s --du --noreport -I 'lost+found' -J --sort=size"
        stdin, stdout, stderr = client.exec_command(command)
        raw_data = stdout.read().decode()
        event.set()
        # Schedule the process_raw_data function to run after a delay
        sub2 = threading.Thread(target=process_raw_data, args=(raw_data,))
        sub2.start()

    # Start the subthread to fetch the data
    sub1 = threading.Thread(target=get_data_remote, daemon=True)
    sub1.start()

    # Continue main thread execution without waiting for subthread
    # Perform other tasks here
    print("Continue main thread execution...")

# handle_data('/mnt/DATA2')