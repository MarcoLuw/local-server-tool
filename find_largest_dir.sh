#!/bin/bash

find_largest_directory() {
    local current_dir=$1
    local largest_dir=""
    local largest_size=0

    # Find the largest subdirectory within the current directory
    while IFS= read -r subdirectory; do
        size=$(du -s "$subdirectory" 2>/dev/null | awk '{print $1}')

        if [[ $size -gt $largest_size ]]; then
            largest_size=$size
            largest_dir=$subdirectory
        fi
    done < <(find "$current_dir" -mindepth 1 -maxdepth 1 -type d 2>/dev/null)

    # Output the largest subdirectory and continue searching within it
    if [[ -n $largest_dir ]]; then
        echo "$largest_dir $largest_size"
        find_largest_directory "$largest_dir"
    fi
}

# Start searching from the directory provided as a command-line argument
start_directory="$1"
echo "$start_directory 0"  # Initial directory with size 0
find_largest_directory "$start_directory"